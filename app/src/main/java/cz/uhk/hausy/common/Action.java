package cz.uhk.hausy.common;

/**
 * Created by kristyna on 28/06/2017.
 */

public enum Action {

    LOG_IN, SIGN_UP

}
