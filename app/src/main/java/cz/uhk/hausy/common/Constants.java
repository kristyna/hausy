package cz.uhk.hausy.common;

/**
 * Created by kristyna on 26/06/2017.
 */

public class Constants {

    public static final String FONT_LIGHT = "fonts/code_pro_light.otf";
    public static final String FONT_BOLD = "fonts/code_pro_bold.otf";
    public static final String FONT_AWESOME = "fonts/FontAwesome.otf";
    public static final String FONT_REGULAR = "fonts/Lintel-Regular.otf";
    public static final String FONT_HAUSY = "fonts/oneday.ttf";

}
