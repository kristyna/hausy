package cz.uhk.hausy.common;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kristyna on 30/06/2017.
 */

public enum DeviceType {

    NONE(-1), SENSOR(0), SWITCH(1), BUTTON(2), MOTION(3);

    private static final Map<Integer, DeviceType> types = new HashMap<>();
    private final int value;
    
    static {
        for (DeviceType type : DeviceType.values()) {
            types.put(type.value, type);
        }
    }

    DeviceType(int value) {
        this.value = value;
    }

    public static DeviceType getType(int i) {
        return types.get(i);
    }

    public int getValue() {
        return value;
    }

}
