package cz.uhk.hausy.app;

import android.app.Application;
import com.octo.android.robospice.SpiceManager;

import org.androidannotations.annotations.EApplication;

import cz.uhk.hausy.io.net.RequestService_;
import cz.uhk.hausy.utils.FontCache;
import io.realm.Realm;

/**
 * Created by kristyna on 26/06/17.
 */

@EApplication
public class HausyApplication extends Application {

    private SpiceManager manager = new SpiceManager(RequestService_.class);

    @Override
    public void onCreate() {
        super.onCreate();
        manager.start(this);
        Realm.init(this);
        FontCache.initFonts(this);
    }


    @Override
    public void onTerminate() {
        super.onTerminate();
        if (manager.isStarted())
            manager.shouldStop();
    }

}
