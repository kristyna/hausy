package cz.uhk.hausy.ui.settings;

import android.app.Fragment;
import android.preference.PreferenceActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

import cz.uhk.hausy.R;
import cz.uhk.hausy.ui.base.BaseActivity;

/**
 * Created by kristyna on 08/08/2017.
 */

@EActivity(R.layout.activity_settings)
public class SettingsActivity extends BaseActivity {

    @AfterViews
    void init() {
        Fragment fragment = SettingsFragment_.builder().build();
        getFragmentManager().beginTransaction().replace(
                R.id.container,
                fragment,
                fragment.getTag()
        ).commit();
    }

}