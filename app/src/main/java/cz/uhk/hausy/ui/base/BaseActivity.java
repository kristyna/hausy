package cz.uhk.hausy.ui.base;


import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.octo.android.robospice.SpiceManager;
import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import cz.uhk.hausy.R;
import cz.uhk.hausy.interfaces.OnBackPressedListener;
import cz.uhk.hausy.io.net.RequestService_;
import cz.uhk.hausy.ui.views.MenuView;
import cz.uhk.hausy.ui.views.MenuView_;
import cz.uhk.hausy.utils.DesignUtils;
import cz.uhk.hausy.utils.animations.MenuTransformation;

/**
 * Created by kristyna on 26/06/17.
 */

@EActivity
public class BaseActivity extends AppCompatActivity {

    private SpiceManager manager = new SpiceManager(RequestService_.class);
    private OnBackPressedListener onBackPressedListener;


    @Override
    protected void onStart() {
        super.onStart();
        manager.start(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(manager.isStarted()) {
            manager.shouldStop();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    public void showFragment(BaseFragment fragment, boolean backStack) {
        showFragment(fragment, backStack, R.id.container);
    }

    public void showFragment(BaseFragment fragment, boolean backStack, int container) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        if (backStack)
            transaction.addToBackStack(fragment.getBackStackTag());

        transaction.replace(container, fragment, fragment.getBackStackTag()).commit();
    }

    public SpiceManager getManager() {
        return manager;
    }

    public void setOnBackPressedListener(OnBackPressedListener onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }

    @Override
    public void onBackPressed() {
        if (onBackPressedListener != null)
            onBackPressedListener.backPressed();
        else
            super.onBackPressed();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(DesignUtils.getThemeId(this));
    }

}
