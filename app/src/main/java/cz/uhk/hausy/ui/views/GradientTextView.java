package cz.uhk.hausy.ui.views;

import android.content.Context;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EView;
import org.androidannotations.annotations.res.ColorRes;

import cz.uhk.hausy.R;
import cz.uhk.hausy.utils.DesignUtils;
import cz.uhk.hausy.utils.FontCache;

/**
 * Created by kristyna on 26/03/2017.<br /><br />
 *
 * BaseTextView handles different typefaces specified in {@link FontCache}.
 * For using specific typeface use custom XML attribute <code>int fontType</code>. Possible values and theirs definition can be found in {@link FontCache}.
 */

@EView
public class GradientTextView extends BaseTextView {

    public GradientTextView(Context context) {
        super(context);
    }

    public GradientTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GradientTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @AfterViews
    void init() {
        Shader textShader = new LinearGradient(
                0, 15, 0, getPaint().getTextSize(),
                new int[]{
                        DesignUtils.fetchColorFromTheme(getContext(), R.attr.colorAccent),
                        DesignUtils.fetchColorFromTheme(getContext(), R.attr.colorPrimaryDark)
                },
                new float[]{0, 1}, Shader.TileMode.CLAMP);

        getPaint().setShader(textShader);
    }

}
