package cz.uhk.hausy.ui.device.picker;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.EView;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.res.DimensionPixelSizeRes;
import org.androidannotations.annotations.res.DrawableRes;
import org.androidannotations.annotations.res.StringArrayRes;

import java.util.Arrays;

import cz.uhk.hausy.R;
import cz.uhk.hausy.ui.base.RecyclerViewAdapterBase;
import cz.uhk.hausy.ui.views.BaseTextView;
import cz.uhk.hausy.ui.views.ViewWrapper;
import cz.uhk.hausy.utils.FontCache;

/**
 * Created by kristyna on 14/09/2017.
 *
 * Handles all necessary steps for creating and displaying icon picker.
 * This class includes also RecyclerView adapter, icon holder view and interface for getting value.
 */

@EView
public class IconPicker extends RecyclerView {

    @Bean
    IconAdapter adapter;

    @StringArrayRes(R.array.iconPicker) String[] icons;
    @DimensionPixelSizeRes(R.dimen.brick_padding) int padding;

    public IconPicker(Context context) {
        super(context);
    }

    public IconPicker(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public IconPicker(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @AfterViews
    void init() {
        adapter.setItems(Arrays.asList(icons));
        setLayoutManager(new GridLayoutManager(getContext(), 4));
        setAdapter(adapter);
        setPadding(padding, padding, padding, padding);
    }


    /**
     * Returns currently selected value (icon)
     */
    public interface IconPickerListener {

        void onIconChange(String icon);
        String getSelectedIcon();

    }



    /**
     * Icon RecyclerView adapter
     */

    @EBean
    public static class IconAdapter extends RecyclerViewAdapterBase<String, IconHolder> {

        private int currPos;
        private IconPickerListener listener;

        @RootContext
        Context context;

        @Override
        protected IconHolder onCreateItemView(ViewGroup parent, int viewType) {
            return IconPicker_.IconHolder_.build(context);
        }

        @Override
        public void onBindViewHolder(final ViewWrapper<IconHolder> holder, int position) {
            final IconHolder view = holder.getView();
            final String icon = getItems().get(position);

            view.setSelected(position == currPos);

            view.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    notifyItemChanged(currPos);
                    currPos = holder.getAdapterPosition();
                    listener.onIconChange(icon);
                    notifyItemChanged(currPos);
                }
            });

            view.bind(icon);
        }

        private void setListener(IconPickerListener listener) {
            this.listener = listener;
        }

    }


    /**
     * Recycler item View capable of converting String to a FontAwesome icon
     */

    @EView
    public static class IconHolder extends BaseTextView {

        @DimensionPixelSizeRes(R.dimen.icon_padding) int padding;
        @DrawableRes(R.drawable.selector_icon_picker) Drawable iconSelector;

        public IconHolder(Context context) {
            super(context);

            setTypeface(FontCache.getFont(FontCache.FONT_AWESOME));
            setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
            setGravity(Gravity.CENTER);
        }

        @AfterViews
        void init() {
            setPadding(padding, padding, padding, padding);
            setBackgroundDrawable(iconSelector);
        }

        public void bind(String icon) {
            setText(icon);
            setTextColor(isSelected() ? Color.WHITE : Color.BLACK);
        }

    }

    public void setListener(IconPickerListener listener) {
        adapter.setListener(listener);

        for (int i = 0; i < icons.length; i++) {
            if (icons[i].equals(listener.getSelectedIcon())) {
                adapter.currPos = i;
                return;
            }
        }
    }

}
