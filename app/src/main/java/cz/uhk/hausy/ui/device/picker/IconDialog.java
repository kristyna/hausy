package cz.uhk.hausy.ui.device.picker;

import android.support.v4.app.DialogFragment;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import cz.uhk.hausy.R;

/**
 * Created by kristyna on 14/09/2017.
 */

@EFragment(R.layout.fragment_dialog_picker)
public class IconDialog extends DialogFragment implements IconPicker.IconPickerListener {

    private IconPicker.IconPickerListener listener;

    @ViewById
    IconPicker iconPicker;

    @AfterViews
    void init() {
        setCancelable(true);
        iconPicker.setListener(this);
    }


    public void setListener(IconPicker.IconPickerListener listener) {
        this.listener = listener;
    }

    @Override
    public void onIconChange(String icon) {
        if (listener != null)
            listener.onIconChange(icon);
        dismiss();
    }

    @Override
    public String getSelectedIcon() {
        return (listener != null) ? listener.getSelectedIcon() : "";
    }

}
