package cz.uhk.hausy.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import cz.uhk.hausy.R;
import cz.uhk.hausy.interfaces.MenuListener;
import cz.uhk.hausy.ui.dashboard.DashboardFragment_;
import cz.uhk.hausy.ui.views.MenuView;
import cz.uhk.hausy.ui.views.MenuView_;
import cz.uhk.hausy.utils.animations.MenuTransformation;

@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity implements MenuListener {

    private SlidingRootNav slidingRoot;
    private boolean isBackpress;

    @ViewById
    Toolbar toolbar;

    @ViewById
    ImageView menu;

    @Click
    void menu() {
        if (isBackpress)
            onBackPressed();
        else if (slidingRoot.isMenuHidden())
            slidingRoot.openMenu();
        else
            slidingRoot.closeMenu();
    }

    @AfterViews
    void init() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);

        showFragment(DashboardFragment_.builder().build(), false);
        navigationLayerInit();
    }

    private void navigationLayerInit() {
        MenuView menuView = MenuView_.build(this);
        menuView.setMenuListener(this);

        MenuTransformation menuTransformation = new MenuTransformation(this);

        slidingRoot = new SlidingRootNavBuilder(this)
                .withMenuOpened(false)
                .withMenuView(menuView)
                .addRootTransformation(menuTransformation)
                .inject();
    }

    @Override
    public void changeFragment(BaseFragment fragment) {
        showFragment(fragment, false);
        slidingRoot.closeMenu();
    }

    @Override
    public void onBackPressed() {
        if (!slidingRoot.isMenuHidden())
            slidingRoot.closeMenu();
        else {
            changeActionBarIcon(R.drawable.ic_menu);
            super.onBackPressed();
        }
    }

    public void changeActionBarIcon(int resource) {
        isBackpress = resource != R.drawable.ic_menu;
        menu.setImageResource(resource);
    }

}
