package cz.uhk.hausy.ui.base;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.octo.android.robospice.SpiceManager;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.res.DrawableRes;

import java.util.List;

import cz.uhk.hausy.R;
import cz.uhk.hausy.utils.DesignUtils;
import cz.uhk.hausy.utils.RealmHelper;
import io.realm.Realm;


/**
 * Created by kristyna on 26/06/2017.
 */

@EFragment
public class BaseFragment extends Fragment implements Validator.ValidationListener {

    public static String TAG = "base_fragment";

    private Validator validator;
    private Context context;
    private Realm realm;
    private int accentColor;

    @DrawableRes(R.drawable.ic_error)
    public Drawable errorIcon;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        validator = new Validator(this);
        validator.setValidationListener(this);

        realm = Realm.getDefaultInstance();

        accentColor = DesignUtils.fetchColorFromTheme(getContext(), R.attr.colorAccent);
    }

    public SpiceManager getManager() {
        return getBaseActivity().getManager();
    }

    public BaseActivity getBaseActivity() {
        return (BaseActivity)context;
    }

    protected String getBackStackTag() {
        return TAG;
    }

    @Override
    public void onValidationSucceeded() {

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            String message = error.getCollatedErrorMessage(context);

            // Change icon color based on theme accent color
            errorIcon.setBounds(0, 0, errorIcon.getIntrinsicWidth(), errorIcon.getIntrinsicHeight());
            errorIcon.setColorFilter(accentColor, PorterDuff.Mode.LIGHTEN);

            View view = error.getView();
            if (view instanceof EditText)
                ((EditText)view).setError(message, errorIcon);
            else if (view instanceof Spinner)
                ((TextView)((Spinner)view).getSelectedView()).setError(message, errorIcon);
            else
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }

    public Realm getRealm() {
        return realm;
    }

    public Validator getValidator() {
        return validator;
    }

    public void makeToast(String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }

}
