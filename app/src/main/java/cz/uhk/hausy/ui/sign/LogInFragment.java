package cz.uhk.hausy.ui.sign;

import android.widget.EditText;

import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import cz.uhk.hausy.R;
import cz.uhk.hausy.interfaces.preferences.UserPrefs_;
import cz.uhk.hausy.ui.base.BaseFragment;
import cz.uhk.hausy.ui.base.MainActivity_;

@EFragment(R.layout.fragment_log_in)
public class LogInFragment extends BaseFragment {

    @NotEmpty
    @ViewById
    EditText username;

    @NotEmpty
    @ViewById
    EditText password;

    @Pref
    UserPrefs_ userPrefs;

    @Click
    void logIn() {
        getValidator().validate();
    }

    @Click
    void signIn() {
        getBaseActivity().showFragment(SignUpFragment_.builder().build(), true);
    }

    @AfterViews
    void init() {
        username.setText("Kristyna");
        password.setText("Admin");
    }

    @Override
    public void onValidationSucceeded() {
        // TODO api request
        // TODO after successful api call
        userPrefs.edit()
                .username().put(username.getText().toString())
                .role().put("Admin")
                .apply();

        MainActivity_.intent(getContext()).start();
    }

}
