package cz.uhk.hausy.ui.dashboard.holders;

import android.content.Context;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;

import cz.uhk.hausy.R;
import cz.uhk.hausy.io.model.Device;
import cz.uhk.hausy.ui.views.BrickView;

/**
 * Created by kristyna on 30/06/2017.
 */

@EViewGroup(R.layout.dashboard_brick_switch)
public class BrickSwitchHolder extends BrickView {

    @ViewById
    TextView brickTitle;

    @ViewById
    TextView brickValue;

    @ViewById
    TextView brickIcon;

    @StringRes String on;
    @StringRes String off;

    public BrickSwitchHolder(Context context) {
        super(context);
    }

    public void bind(Device device) {
        brickTitle.setText(device.getName());
        brickValue.setText(device.getRequiredValue());
        brickIcon.setText(device.getIcon());
    }

}
