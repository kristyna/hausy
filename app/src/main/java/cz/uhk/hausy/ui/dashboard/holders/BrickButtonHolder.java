package cz.uhk.hausy.ui.dashboard.holders;

import android.content.Context;
import android.widget.Button;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import cz.uhk.hausy.R;
import cz.uhk.hausy.io.model.Device;
import cz.uhk.hausy.ui.views.BrickView;

/**
 * Created by kristyna on 5/06/2017.
 */

@EViewGroup(R.layout.dashboard_brick_button)
public class BrickButtonHolder extends BrickView {

    @ViewById
    TextView brickTitle;

    @ViewById
    Button brickButton;

    @ViewById
    TextView brickIcon;

    public BrickButtonHolder(Context context) {
        super(context);
    }

    public void bind(Device device) {
        brickTitle.setText(device.getName());
        brickButton.setText(device.getRequiredValue());
        brickIcon.setText(device.getIcon());
    }

}
