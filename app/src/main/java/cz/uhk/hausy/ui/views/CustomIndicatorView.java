package cz.uhk.hausy.ui.views;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.github.paolorotolo.appintro.IndicatorController;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.hausy.R;
import cz.uhk.hausy.utils.DesignUtils;


/**
 * Created by kristyna on 26/03/2017.
 */

public class CustomIndicatorView implements IndicatorController {
    private static final int FIRST_PAGE_NUM = 0;
    int mCurrentPosition;
    private Context mContext;
    private LinearLayout mDotLayout;
    private List<ImageView> mDots;
    private int mSlideCount;

    @Override
    public View newInstance(@NonNull Context context) {
        mContext = context;
        mDotLayout = (LinearLayout) View.inflate(context, com.github.paolorotolo.appintro.R.layout.default_indicator, null);

        return mDotLayout;
    }

    @Override
    public void initialize(int slideCount) {
        mDots = new ArrayList<>();
        mSlideCount = slideCount;

        for (int i = 0; i < slideCount; i++) {
            ImageView dot = new ImageView(mContext);
            dot.setImageDrawable(ContextCompat.getDrawable(mContext, DesignUtils.fetchResourceFromTheme(mContext, R.attr.unselectedIndicator)));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            mDotLayout.addView(dot, params);
            mDots.add(dot);
        }
        selectPosition(FIRST_PAGE_NUM);
    }

    @Override
    public void selectPosition(int index) {
        mCurrentPosition = index;
        for (int i = 0; i < mSlideCount; i++) {
            int drawableId = (i == index) ?
                    (R.drawable.indicator_selected) : (DesignUtils.fetchResourceFromTheme(mContext, R.attr.unselectedIndicator));

            mDots.get(i).setImageDrawable(mContext.getResources().getDrawable(drawableId));
        }
    }

    @Override
    public void setSelectedIndicatorColor(int color) {
    }

    @Override
    public void setUnselectedIndicatorColor(int color) {
    }
}
