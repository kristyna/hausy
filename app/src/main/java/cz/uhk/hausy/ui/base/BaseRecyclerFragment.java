package cz.uhk.hausy.ui.base;

import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.OptionsMenuItem;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.hausy.R;
import cz.uhk.hausy.interfaces.BaseRecyclerListener;
import cz.uhk.hausy.interfaces.RecyclerFragmentListener;
import cz.uhk.hausy.interfaces.ISelectable;
import cz.uhk.hausy.io.model.Selectable;
import cz.uhk.hausy.ui.base.adapter.BaseRecyclerAdapter;
import cz.uhk.hausy.utils.GeneralUtils;
import cz.uhk.hausy.utils.SimpleDividerItemDecoration;

/**
 * Created by kristyna on 16/07/2017.
 */

@EFragment
@OptionsMenu(R.menu.menu_search)
public class BaseRecyclerFragment<Item extends ISelectable> extends BaseFragment implements SearchView.OnQueryTextListener, BaseRecyclerListener<Item> {

    private List<Item> selectables;
    private ActionMode.Callback actionCallback;
    private RecyclerFragmentListener<Item> recyclerlistener;

    @OptionsMenuItem
    public MenuItem search;

    @ViewById
    public RecyclerView recycler;

    @Bean
    public BaseRecyclerAdapter<Item> adapter;

    @AfterViews
    public void init() {
        setHasOptionsMenu(true);

        // Setup recycler & adapter
        selectables = recyclerlistener.getSelectables();

        adapter.setItems(selectables);
        adapter.setBaseRecyclerListener(this);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        recycler.addItemDecoration(new SimpleDividerItemDecoration(getContext()));
        recycler.setAdapter(adapter);

        setupActionMode();
    }

    private void setupActionMode() {
        actionCallback = new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.menu_options, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                if (item.getItemId() == R.id.delete)
                    for (int i = 0; i < adapter.getItems().size(); i++) {
                        Selectable selectable = adapter.getItems().get(i).getSelectable();
                        if (selectable.isSelected()) {
                            selectables.remove(selectable);
                            adapter.notifyItemRemoved(i);
                            recyclerlistener.onDelete(selectable);
                        }
                    }
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                adapter.endEdit();
            }
        };
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Setup search view
        SearchView searchView = (SearchView) search.getActionView();
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        List<Item> filtered = new ArrayList<>();
        for (Item selectable : selectables) {
            if (GeneralUtils.contains(selectable.getSelectable().getName(), newText))
                filtered.add(selectable);
        }

        adapter.setItems(filtered);
        adapter.notifyDataSetChanged();

        return true;
    }

    @Override
    public void onClick(Item selectable) {
        recyclerlistener.onRecyclerItemClick(selectable);
    }

    @Override
    public void onStartEdit() {
        getBaseActivity().startSupportActionMode(actionCallback);
    }

    public void setRecyclerlistener(RecyclerFragmentListener<Item> recyclerlistener) {
        this.recyclerlistener = recyclerlistener;
    }

}
