package cz.uhk.hausy.ui.dashboard;

import android.graphics.Color;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import cz.uhk.hausy.R;
import cz.uhk.hausy.interfaces.ItemDragListener;
import cz.uhk.hausy.interfaces.OnBrickClickListener;
import cz.uhk.hausy.interfaces.preferences.UserPrefs_;
import cz.uhk.hausy.io.model.Device;
import cz.uhk.hausy.io.model.History;
import cz.uhk.hausy.io.net.requests.DevicesRequest;
import cz.uhk.hausy.ui.base.BaseFragment;
import cz.uhk.hausy.ui.dashboard.adapters.DashboardAdapter;
import cz.uhk.hausy.ui.device.DeviceDetailFragment_;
import cz.uhk.hausy.ui.views.ViewWrapper;
import cz.uhk.hausy.utils.DesignUtils;
import cz.uhk.hausy.utils.RealmHelper;
import cz.uhk.hausy.utils.design.GridDecoration;
import cz.uhk.hausy.utils.SimpleItemTouchHelperCallback;

/**
 * Created by kristyna on 29/06/2017.
 */

@EFragment(R.layout.fragment_dashboard)
@OptionsMenu(R.menu.menu_main)
public class DashboardFragment extends BaseFragment implements OnBrickClickListener, ItemDragListener {

    private ItemTouchHelper itemTouchHelper;
    private boolean editMode = false;

    @ViewById
    RecyclerView dashboardRecycler;

    @Pref
    UserPrefs_ userPrefs;

    @Bean
    DashboardAdapter adapter;

    @ViewById
    GraphView graph;

    @OptionsItem(R.id.grid)
    void changeGrid() {
        editMode = !editMode;
        if (editMode) {
            if (itemTouchHelper == null) {
                ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
                itemTouchHelper = new ItemTouchHelper(callback);
            }

            itemTouchHelper.attachToRecyclerView(dashboardRecycler);
            adapter.setDragListener(this);
        } else {
            itemTouchHelper.attachToRecyclerView(null);
            adapter.setDragListener(null);

            JsonArray order = new JsonArray();
            for (Device device : adapter.getItems()) {
                order.add(new JsonPrimitive(device.getId()));
            }

            userPrefs.dashboard().put(order.toString());
        }
    }

    @AfterViews
    void init() {
        setHasOptionsMenu(true);

        adapter.setClickListener(this);
        dashboardRecycler.setHasFixedSize(true);
        dashboardRecycler.setLayoutManager(new GridLayoutManager(getContext(), 2));
        dashboardRecycler.setAdapter(adapter);
        dashboardRecycler.addItemDecoration(new GridDecoration(getContext(), 10, 2));

        getManager().execute(new DevicesRequest(), new RequestListener<List>() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {}

            @Override
            public void onRequestSuccess(List list) {
                if (list == null) {
                    Log.d("device", "null");
                    return;
                }

                List<Device> devices = (List<Device>) list;
                for (Device device : devices)
                    device.prepare();

                arrangeDashboard(devices);
                adapter.setItems(devices);
                adapter.notifyDataSetChanged();

                RealmHelper.insertOrUpdate(getRealm(), devices);
            }
        });
    }

    /**
     * Sorts dashboard bricks according to your previous setting stored in preferences
     */
    private void arrangeDashboard(List<Device> devices) {
        JsonArray ids = new JsonParser().parse(userPrefs.dashboard().get()).getAsJsonArray();
        int missingDevicesCount = 0;
        for (int i = 0; i < ids.size(); i++)
            for (int j = 0; j < devices.size(); j++)
                if (devices.get(j).getId() == ids.get(i).getAsInt()) {
                    missingDevicesCount -= j;
                    Collections.swap(devices, i - missingDevicesCount/devices.size(), j);
                    break;
                } else
                    missingDevicesCount++;
    }

    @Override
    public void onValueClick(Device device) {
        getBaseActivity().showFragment(DeviceDetailFragment_.builder().device(device).build(), true);
    }

    @Override
    public void onSwitchClick(Device device) {
        getBaseActivity().showFragment(DeviceDetailFragment_.builder().device(device).build(), true);
    }

    @Override
    public void onButtonClick(Device device) {
        Toast.makeText(getContext(), "button", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLightsClick(Device device) {
        Toast.makeText(getContext(), "lights", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void drag(ViewWrapper viewHolder) {
        itemTouchHelper.startDrag(viewHolder);
    }

}
