package cz.uhk.hausy.ui.views;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import cz.uhk.hausy.R;
import cz.uhk.hausy.utils.DesignUtils;
import cz.uhk.hausy.utils.FontCache;

/**
 * Created by kristyna on 06/04/2017.<br /><br />
 *
 * BaseTextView handles different typefaces specified in {@link FontCache}.
 * For using specific typeface use custom XML attribute <code>int etFontType</code>. Possible values and theirs definition can be found in {@link FontCache}.
 */

public class BaseEditText extends AppCompatEditText {

    public BaseEditText(Context context) {
        super(context);
    }

    public BaseEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        setTypeface(DesignUtils.applyCustomFont(context, attrs, R.styleable.BaseEditText, R.styleable.BaseEditText_etFontType));
    }

    public BaseEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        setTypeface(DesignUtils.applyCustomFont(context, attrs, R.styleable.BaseEditText, R.styleable.BaseEditText_etFontType));
    }

}
