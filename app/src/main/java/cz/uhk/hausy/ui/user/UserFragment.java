package cz.uhk.hausy.ui.user;

import android.os.Bundle;
import android.support.annotation.Nullable;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;

import java.util.List;

import cz.uhk.hausy.R;
import cz.uhk.hausy.interfaces.RecyclerFragmentListener;
import cz.uhk.hausy.io.model.Selectable;
import cz.uhk.hausy.io.model.User;
import cz.uhk.hausy.ui.base.BaseRecyclerFragment;
import cz.uhk.hausy.utils.RealmHelper;

/**
 * Created by kristyna on 07/07/2017.
 */

@EFragment(R.layout.fragment_recycler)
public class UserFragment extends BaseRecyclerFragment<User> implements RecyclerFragmentListener<User> {

    @Click
    void add() {
        getBaseActivity().showFragment(UserDetailFragment_.builder().build(), true);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRecyclerlistener(this);
    }

    @Override
    public List<User> getSelectables() {
        return RealmHelper.get(getRealm(), User.class);
    }

    @Override
    public void onDelete(Selectable selectable) {
        // TODO api
    }

    @Override
    public void onRecyclerItemClick(User user) {
        getBaseActivity().showFragment(UserDetailFragment_.builder().user(user).build(), true);
    }

}
