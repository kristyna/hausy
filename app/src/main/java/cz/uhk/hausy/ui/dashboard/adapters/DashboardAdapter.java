package cz.uhk.hausy.ui.dashboard.adapters;

import android.content.Context;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.Collections;
import java.util.List;

import cz.uhk.hausy.R;
import cz.uhk.hausy.common.DeviceType;
import cz.uhk.hausy.interfaces.ItemDragListener;
import cz.uhk.hausy.interfaces.OnBrickClickListener;
import cz.uhk.hausy.interfaces.ItemTouchListener;
import cz.uhk.hausy.io.model.Device;
import cz.uhk.hausy.ui.dashboard.holders.BrickButtonHolder;
import cz.uhk.hausy.ui.dashboard.holders.BrickButtonHolder_;
import cz.uhk.hausy.ui.dashboard.holders.BrickLightsHolder;
import cz.uhk.hausy.ui.dashboard.holders.BrickLightsHolder_;
import cz.uhk.hausy.ui.dashboard.holders.BrickSwitchHolder;
import cz.uhk.hausy.ui.dashboard.holders.BrickSwitchHolder_;
import cz.uhk.hausy.ui.dashboard.holders.BrickValueHolder;
import cz.uhk.hausy.ui.dashboard.holders.BrickValueHolder_;
import cz.uhk.hausy.ui.views.ViewWrapper;
import cz.uhk.hausy.utils.animations.AnimationUtils;

/**
 * Created by kristyna on 30/06/2017.
 */

@EBean
public class DashboardAdapter extends RecyclerView.Adapter<ViewWrapper> implements ItemTouchListener {

    @RootContext
    Context context;

    private List<Device> items;
    private OnBrickClickListener listener;
    private ItemDragListener dragListener;

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    @Override
    public final ViewWrapper onCreateViewHolder(ViewGroup parent, int viewType) {
        DeviceType type = getBrickType(viewType);

        switch (type) {
            case SENSOR: return new ViewWrapper<>(BrickValueHolder_.build(context));
            case SWITCH: return new ViewWrapper<>(BrickSwitchHolder_.build(context));
            case BUTTON: return new ViewWrapper<>(BrickButtonHolder_.build(context));
            case MOTION: return new ViewWrapper<>(BrickLightsHolder_.build(context));

            default: return null;
        }
    }

    @Override
    public void onBindViewHolder(final ViewWrapper holder, final int position) {
        final View view = holder.getView();
        final DeviceType type = getBrickType(holder.getItemViewType());
        if (dragListener != null)
            AnimationUtils.animate(context, view, R.anim.dashboard_shake);
        else
            view.clearAnimation();

        final Device device = items.get(position);

        switch (type) {
            case SENSOR: ((BrickValueHolder)view).bind(device); break;
            case SWITCH: ((BrickSwitchHolder)view).bind(device); break;
            case BUTTON: ((BrickButtonHolder)view).bind(device); break;
            case MOTION: ((BrickLightsHolder)view).bind(device); break;
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener == null)
                    return;

                switch (type) {
                    case SENSOR: listener.onValueClick(device); break;
                    case SWITCH: listener.onSwitchClick(device); break;
                    case BUTTON: listener.onButtonClick(device); break;
                    case MOTION: listener.onLightsClick(device); break;
                }
            }
        });

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (dragListener != null && MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                    dragListener.drag(holder);
                }
                return false;
            }
        });

    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType();
    }

    public void setItems(List<Device> items) {
        this.items = items;
    }

    public void setClickListener(OnBrickClickListener listener) {
        this.listener = listener;
    }

    public void setDragListener(ItemDragListener dragListener) {
        this.dragListener = dragListener;
        // Redraw items to start animation
        notifyDataSetChanged();
    }

    private static DeviceType getBrickType(int type) {
        return DeviceType.getType(type);
    }

    @Override
    public boolean onItemMove(int from, int to) {
        Collections.swap(items, from, to);
        notifyItemMoved(from, to);
        return true;
    }

    @Override
    public void onItemDismiss(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    public List<Device> getItems() {
        return items;
    }

}