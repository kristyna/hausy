package cz.uhk.hausy.ui.walkthrough;

import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import cz.uhk.hausy.R;
import cz.uhk.hausy.ui.base.BaseFragment;

/**
 * Created by kristyna on 25/03/2017.
 */

@EFragment(R.layout.fragment_walkthrough_content)
public class WalkthroughFragment extends BaseFragment {

    @FragmentArg
    String title;

    @FragmentArg
    String desc;

    @ViewById(R.id.title)
    TextView tvTitle;

    @ViewById(R.id.description)
    TextView tvDesc;

    @AfterViews
    void init() {
        tvTitle.setText(title);
        tvDesc.setText(desc);
    }

}
