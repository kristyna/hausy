package cz.uhk.hausy.ui.base.menu;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import cz.uhk.hausy.R;
import cz.uhk.hausy.ui.views.BaseTextView;


/**
 * Created by yarolegovich on 25.03.2017.
 */

public class SimpleItem extends DrawerItem<SimpleItem.ViewHolder> {

    private String icon;
    private String title;

    public SimpleItem(String icon, String title) {
        this.icon = icon;
        this.title = title;
    }

    @Override
    public ViewHolder createViewHolder(ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_menu, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void bindViewHolder(ViewHolder holder) {
        holder.title.setText(title);
        holder.icon.setText(icon);

        holder.root.setBackgroundResource(isChecked ? R.color.transparentBlack : 0);
    }

    static class ViewHolder extends DrawerAdapter.ViewHolder {

        private BaseTextView icon;
        private BaseTextView title;
        private LinearLayout root;

        ViewHolder(View itemView) {
            super(itemView);
            icon = (BaseTextView) itemView.findViewById(R.id.icon);
            title = (BaseTextView) itemView.findViewById(R.id.title);
            root = (LinearLayout) itemView.findViewById(R.id.root);
        }
    }

}
