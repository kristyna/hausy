package cz.uhk.hausy.ui.settings;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.util.Log;
import android.widget.Toast;

import org.androidannotations.annotations.AfterPreferences;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.PreferenceByKey;
import org.androidannotations.annotations.PreferenceChange;
import org.androidannotations.annotations.PreferenceClick;
import org.androidannotations.annotations.PreferenceScreen;
import org.androidannotations.annotations.res.StringRes;

import cz.uhk.hausy.R;
import cz.uhk.hausy.interfaces.DialogButtonListener;
import cz.uhk.hausy.utils.DialogUtils;
import cz.uhk.hausy.utils.GeneralUtils;

/**
 * Created by kristyna on 08/08/2017.
 */

@PreferenceScreen(R.xml.settings)
@EFragment
public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    @StringRes(R.string.key_theme)
    String theme;

    @AfterPreferences
    void initPrefs() {
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        if (s.equals(theme) && getActivity() != null) {
            DialogUtils.showDialog(
                    getActivity(),
                    R.string.restart_application,
                    R.string.do_you_want_to_restart_application,
                    R.string.yes,
                    R.string.do_it_later,
                    new DialogButtonListener() {
                        @Override
                        public void positiveButtonClick() {
                            restartApp();
                        }
                    }
            );
        }
    }

    private void restartApp() {
        Intent i = getActivity().getBaseContext().getPackageManager()
                .getLaunchIntentForPackage( getActivity().getBaseContext().getPackageName() );
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

}
