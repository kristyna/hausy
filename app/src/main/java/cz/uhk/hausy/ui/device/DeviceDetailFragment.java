package cz.uhk.hausy.ui.device;

import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.SeekBarProgressChange;
import org.androidannotations.annotations.ViewById;

import java.text.SimpleDateFormat;

import cz.uhk.hausy.R;
import cz.uhk.hausy.common.DeviceType;
import cz.uhk.hausy.io.model.Device;
import cz.uhk.hausy.io.model.History;
import cz.uhk.hausy.io.net.requests.UpdateDeviceValueRequest;
import cz.uhk.hausy.ui.base.BaseFragment;
import cz.uhk.hausy.ui.base.MainActivity;
import cz.uhk.hausy.utils.DesignUtils;
import cz.uhk.hausy.utils.GeneralUtils;

/**
 * Created by kristyna on 19/09/2017.
 */

@EFragment(R.layout.fragment_device_detail)
public class DeviceDetailFragment extends BaseFragment {

    private DeviceType type;

    @FragmentArg
    Device device;

    @ViewById
    TextView deviceName;

    @ViewById
    GraphView graph;


    /**
     *  SENSOR
     */

    @ViewById
    View sensorLayout;

    @ViewById
    TextView seekBarProgress;

    @ViewById
    SeekBar seekBar;

    @ViewById
    TextView seekBarMin;

    @ViewById
    TextView seekBarMax;


    /**
     *  SWITCH
     */

    @ViewById
    View switchLayout;

    @ViewById
    Switch switchBtn;

    @ViewById
    TextView status;


    @SeekBarProgressChange(R.id.seekBar)
    void seekbarProgressChange() {
        seekBarProgress.setText(getUnitValue(getValueFromSeekBar()));
    }

    @Click
    void save() {
        UpdateDeviceValueRequest request = new UpdateDeviceValueRequest(
                device.getId(),
                getValue()
        );

        getManager().execute(request, new RequestListener<String>() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {
                // TODO
            }

            @Override
            public void onRequestSuccess(String aVoid) {
                makeToast("Value has been changed");
            }
        });
    }

    @AfterViews
    void init() {
        ((MainActivity)getBaseActivity()).changeActionBarIcon(R.drawable.ic_chevron_left);

        deviceName.setText(device.getName());

        type = DeviceType.getType(device.getType());
        switch (type) {
            case SENSOR:
                setupSensor();
                break;

            case SWITCH:
                setupSwitch();
        }
    }

    private String getValue() {
        switch (type) {
            case SENSOR:
                return getValueFromSeekBar();

            case SWITCH:
                return getSwitchText();
        }

        return "NaN";
    }


    /****
     **  SENSOR methods
     ****/

    private void setupSensor() {
        setupSeekBar();

        seekBarMin.setText(getUnitValue(GeneralUtils.getStringFromDouble(device.getMin())));
        seekBarMax.setText(getUnitValue(GeneralUtils.getStringFromDouble(device.getMax())));

        /*
        TODO is waiting for API
        LineGraphSeries<History> series = new LineGraphSeries<>();
        for (History history : device.getHistory())
            series.appendData(history, false, 10);

        series.setColor(DesignUtils.fetchColorFromTheme(getContext(), R.attr.colorAccent));
        series.setAnimated(true);
        series.setThickness(10);

        graph.getGridLabelRenderer().setNumHorizontalLabels(device.getHistory().size() + 4);
        graph.getViewport().setScrollable(true);

        graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(getActivity(), new SimpleDateFormat("HH:mm")));
        graph.getGridLabelRenderer().setGridColor(DesignUtils.fetchColorFromTheme(getContext(), R.attr.colorPrimaryDark));
        graph.addSeries(series);*/

        sensorLayout.setVisibility(View.VISIBLE);
    }

    private String getUnitValue(String value) {
        return value + " " + device.getUnit();
    }

    /**
     * Sets maximum based on MIN, MAX and STEP values of the device.
     * If no required value is selected, progress is set to be in the middle of the MIN and MAX.
     */
    private void setupSeekBar() {
        seekBar.setMax((int) ((device.getMax() - device.getMin()) / device.getStep()));

        if (device.getRequiredValue() == null || device.getRequiredValue().equals(""))
            seekBar.setProgress(seekBar.getMax()/2);
        else
            seekBar.setProgress(getValueToSeekBar());
    }

    private String getValueFromSeekBar() {
        return String.valueOf(device.getMin() + seekBar.getProgress() * device.getStep());
    }

    /**
     * Maps real required value to SeekBar range.
     *
     * @return int Corresponding value on the SeekBar
     */
    private int getValueToSeekBar() {
       return (int) ((Double.parseDouble(device.getRequiredValue()) - device.getMin()) / device.getStep());
    }


    /****
     **  SWITCH methods
     ****/

    @CheckedChange(R.id.switchBtn)
    void onSwitchChanged() {
        changeSwitchText();
    }

    private void setupSwitch() {
        switchBtn.setChecked(device.getRequiredValue().equalsIgnoreCase(getString(R.string.on)));
        changeSwitchText();
        switchLayout.setVisibility(View.VISIBLE);
    }

    private void changeSwitchText() {
        status.setText(getSwitchText());
    }

    private String getSwitchText() {
        return getString(switchBtn.isChecked() ? R.string.on : R.string.off);
    }

}
