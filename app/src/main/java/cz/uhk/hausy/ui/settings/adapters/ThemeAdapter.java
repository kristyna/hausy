package cz.uhk.hausy.ui.settings.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.res.IntArrayRes;
import org.androidannotations.annotations.res.StringArrayRes;

import cz.uhk.hausy.R;
import cz.uhk.hausy.interfaces.BaseRecyclerListener;
import cz.uhk.hausy.io.model.Selectable;
import cz.uhk.hausy.ui.base.RecyclerViewAdapterBase;
import cz.uhk.hausy.ui.base.holder.BaseHolder;
import cz.uhk.hausy.ui.base.holder.BaseHolder_;
import cz.uhk.hausy.ui.settings.holders.ThemeHolder;
import cz.uhk.hausy.ui.settings.holders.ThemeHolder_;
import cz.uhk.hausy.ui.views.ViewWrapper;

/**
 * Created by kristyna on 08/08/2017.
 */

public class ThemeAdapter extends BaseAdapter {

    private Context context;
    private String[] themes;
    private int[] themeColors;

    public ThemeAdapter(Context context) {
        this.context = context;
        themes = context.getResources().getStringArray(R.array.themes);
        themeColors = context.getResources().getIntArray(R.array.themeColors);
    }

    @Override
    public int getCount() {
        return themes.length;
    }

    @Override
    public Object getItem(int i) {
        return themes[i];
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ThemeHolder themeHolder;
        if (convertView == null)
            themeHolder = ThemeHolder_.build(context);
        else
            themeHolder = (ThemeHolder) convertView;

        themeHolder.bind(
                themes[position],
                themeColors[position*2],
                themeColors[position*2+1]
        );

        return themeHolder;
    }

}
