package cz.uhk.hausy.ui.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.hausy.ui.views.ViewWrapper;


public abstract class RecyclerViewAdapterBase<T, V extends View> extends RecyclerView.Adapter<ViewWrapper<V>> {

    protected List<T> items = new ArrayList<T>();

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public final ViewWrapper<V> onCreateViewHolder(ViewGroup parent, int viewType) {

        ViewWrapper<V> viewWrapper = new ViewWrapper<V>(onCreateItemView(parent, viewType));
        return viewWrapper;
    }

    protected abstract V onCreateItemView(ViewGroup parent, int viewType);

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

}