package cz.uhk.hausy.ui.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import cz.uhk.hausy.R;
import cz.uhk.hausy.utils.CircleTransform;

/**
 * Created by kristyna on 08/06/2017.
 */

@EViewGroup(R.layout.view_rounded_image)
public class RoundedImage extends RelativeLayout {

    @ViewById
    ImageView imageView;

    public RoundedImage(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RoundedImage(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setImageUrl(String url) {
        setImage(Picasso.with(getContext()).load(url));
    }

    public void setImageUri(Uri uri) {
        setImage(Picasso.with(getContext()).load(uri));
    }

    private void setImage(RequestCreator requestCreator) {
        requestCreator.placeholder(R.drawable.ic_add_a_photo)
                .error(R.drawable.ic_add_a_photo)
                .fit().centerCrop()
                .transform(new CircleTransform())
                .into(imageView);
    }

}

