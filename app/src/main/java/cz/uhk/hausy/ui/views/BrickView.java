package cz.uhk.hausy.ui.views;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EView;
import org.androidannotations.annotations.res.DimensionPixelSizeRes;
import org.androidannotations.annotations.res.DrawableRes;

import cz.uhk.hausy.R;
import cz.uhk.hausy.interfaces.HolderTouchListener;
import cz.uhk.hausy.utils.DesignUtils;

/**
 * Created by kristyna on 05/07/2017.
 */

@EView
public class BrickView extends RelativeLayout implements HolderTouchListener {

    private static final float SCALE = 1.1f;
    private static final float DEFAULT_SCALE = 1.0f;

    public @DimensionPixelSizeRes(R.dimen.brick_padding) int padding;

    public BrickView(Context context) {
        super(context);
    }

    @AfterViews
    public void init() {
        setPadding(padding, padding, padding, padding);
        setBackgroundResource(DesignUtils.fetchResourceFromTheme(getContext(), R.attr.borderColorfulRounded));
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    @Override
    public void onItemSelected() {
        setScaleX(SCALE);
        setScaleY(SCALE);
    }

    @Override
    public void onItemClear() {

    }

    @Override
    public void onItemDropped() {
        setScaleX(DEFAULT_SCALE);
        setScaleY(DEFAULT_SCALE);
    }

}
