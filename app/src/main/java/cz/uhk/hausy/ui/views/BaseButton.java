package cz.uhk.hausy.ui.views;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import cz.uhk.hausy.R;
import cz.uhk.hausy.utils.DesignUtils;
import cz.uhk.hausy.utils.FontCache;

/**
 * Created by kristyna on 26/03/2017.<br /><br />
 *
 * BaseButton handles different typefaces specified in {@link FontCache}.
 * For using specific typeface use custom XML attribute <code>int btnFontType</code>. Possible values and theirs definition can be found in {@link FontCache}.
 */

public class BaseButton extends AppCompatButton {

    public BaseButton(Context context) {
        super(context);
    }

    public BaseButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        setTypeface(DesignUtils.applyCustomFont(context, attrs, R.styleable.BaseButton, R.styleable.BaseButton_btnFontType));
    }

    public BaseButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        setTypeface(DesignUtils.applyCustomFont(context, attrs, R.styleable.BaseButton, R.styleable.BaseButton_btnFontType));
    }

}
