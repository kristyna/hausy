package cz.uhk.hausy.ui.walkthrough;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.github.paolorotolo.appintro.AppIntroBase;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.sharedpreferences.Pref;

import cz.uhk.hausy.R;
import cz.uhk.hausy.common.Action;
import cz.uhk.hausy.interfaces.preferences.UserPrefs_;
import cz.uhk.hausy.ui.base.MainActivity_;
import cz.uhk.hausy.ui.sign.LogInActivity_;
import cz.uhk.hausy.ui.views.CustomIndicatorView;
import cz.uhk.hausy.utils.DesignUtils;


@EActivity
public class WalkthroughActivity extends AppIntroBase {

    private static final int PERMISSIONS_REQUEST = 1234;

    @Pref
    UserPrefs_ userPrefs;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(DesignUtils.getSplashThemeId(this));
        super.onCreate(savedInstanceState);

        if (userPrefs.token().exists()) {
            finish();
            MainActivity_.intent(this).start();
            return;
        }

        setCustomIndicator(new CustomIndicatorView());
        addSlide(buildFragment(getString(R.string.wt_first_title), getString(R.string.wt_first_desc)));
        addSlide(buildFragment(getString(R.string.wt_second_title), getString(R.string.wt_second_desc)));
    }

    /*@Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST)
            MainActivity_.intent(WalkthroughActivity.this).start();

        finish();
        // TODO appPreferences.tutorialDone().put(true);
    }*/

    private Fragment buildFragment(String title, String desc) {
        return WalkthroughFragment_.builder().title(title).desc(desc).build();
    }

    @Override
    public void onSkipPressed() {
        finish();
        LogInActivity_.intent(this).action(Action.LOG_IN).start();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_splash_screen;
    }

}
