package cz.uhk.hausy.ui.user;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Select;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ItemSelect;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.ColorRes;
import org.androidannotations.annotations.res.StringRes;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.hausy.R;
import cz.uhk.hausy.interfaces.DialogButtonListener;
import cz.uhk.hausy.interfaces.SpinnerComparator;
import cz.uhk.hausy.io.model.Group;
import cz.uhk.hausy.io.model.User;
import cz.uhk.hausy.ui.base.BaseFragment;
import cz.uhk.hausy.ui.base.MainActivity;
import cz.uhk.hausy.ui.views.RoundedImage;
import cz.uhk.hausy.utils.DialogUtils;
import cz.uhk.hausy.utils.GeneralUtils;
import cz.uhk.hausy.utils.RealmHelper;

import static android.app.Activity.RESULT_OK;

/**
 * Created by kristyna on 17/07/2017.
 */

@EFragment(R.layout.fragment_user_detail)
@OptionsMenu(R.menu.menu_user)
public class UserDetailFragment extends BaseFragment {

    private Group selectedGroup;

    @FragmentArg
    User user;

    @ViewById
    RoundedImage profileImage;

    @ViewById
    @Email
    @NotEmpty
    EditText email;

    @ViewById
    @NotEmpty
    EditText firstname;

    @ViewById
    @NotEmpty
    EditText lastname;

    @ViewById
    @Select
    Spinner group;

    @StringRes(R.string.choose_group) String groupPrompt;
    @ColorRes(R.color.textHintLight) int hintColor;
    @ColorRes(R.color.textDark) int textColor;


    @OptionsItem(R.id.change_password)
    void changePassword() {
        makeToast("TODO change pass");
    }

    @OptionsItem
    void delete() {
        if (user == null)
            return;

        DialogUtils.showDeleteDialog(
                getActivity(),
                getString(R.string.user),
                user.getSelectable().getName(),
                new DialogButtonListener() {
                    @Override
                    public void positiveButtonClick() {
                        RealmHelper.delete(getRealm(), user);
                        getBaseActivity().onBackPressed();
                    }
                }
        );
    }

    @Click({R.id.profileImage, R.id.edit})
    void profileImageClicked() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.OVAL)
                .setAspectRatio(100, 100)
                .setFixAspectRatio(true)
                .start(getContext(), this);
    }

    @ItemSelect(R.id.group)
    void groupItemSelect(boolean selected, Group group) {
        if (selected)
            selectedGroup = group;
    }

    @Click
    void save() {
        getValidator().validate();
    }

    @Override
    public void onValidationSucceeded() {
        User newUser = RealmHelper.detachFromRealm(getRealm(), User.class, user);

        newUser.update(
                email.getText().toString(),
                firstname.getText().toString(),
                lastname.getText().toString(),
                selectedGroup
        );

        RealmHelper.insertOrUpdate(getRealm(), newUser);

        getBaseActivity().onBackPressed();
    }

    @AfterViews
    void init() {
        setHasOptionsMenu(true);
        ((MainActivity)getBaseActivity()).changeActionBarIcon(R.drawable.ic_chevron_left);

        profileImage.setImageUrl("http://assets.nydailynews.com/polopoly_fs/1.1948348.1411403133!/img/httpImage/image.jpg_gen/derivatives/landscape_1200/2015-ford-mustang-gt-headline.jpg");

        List<Group> groups = new ArrayList<>();
        groups.add(new Group(0, groupPrompt));
        groups.add(new Group(1, "Admin"));
        groups.add(new Group(2, "Children"));
        groups.add(new Group(3, "Guests"));

        setAdapter(groups);

        if (user != null)
            initUser();
    }

    private void initUser() {
        email.setText(user.getEmail());
        firstname.setText(user.getFirstname());
        lastname.setText(user.getLastname());

        GeneralUtils.spinnerSetSelection(group, new SpinnerComparator() {
            @Override
            public boolean equals(int position) {
                return ((Group)group.getAdapter().getItem(position)).getId() == user.getGroup().getId();
            }
        });
    }

    private void setAdapter(List<Group> groups) {
        group.setAdapter(new ArrayAdapter<Group>(getContext(), R.layout.item_spinner, groups) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);

                ((TextView)view).setTextColor( position == 0 ? hintColor : textColor);

                return view;
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                profileImage.setImageUri(resultUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.d("Image cropper", error.getMessage());
            }
        }
    }
}
