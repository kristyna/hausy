package cz.uhk.hausy.ui.sign;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.FragmentArg;

import java.util.List;

import cz.uhk.hausy.R;
import cz.uhk.hausy.common.Action;
import cz.uhk.hausy.ui.base.BaseActivity;
import cz.uhk.hausy.utils.DesignUtils;

@EActivity(R.layout.activity_sign)
public class LogInActivity extends BaseActivity {

    @Extra
    Action action;

    @AfterViews
    void init() {
        if (action == null)
            return;

        switch (action) {

            case LOG_IN:
                showFragment(LogInFragment_.builder().build(), false);
                break;
            case SIGN_UP:
                break;
        }

    }

}
