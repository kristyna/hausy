package cz.uhk.hausy.ui.base.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import cz.uhk.hausy.interfaces.BaseRecyclerListener;
import cz.uhk.hausy.interfaces.ISelectable;
import cz.uhk.hausy.ui.base.RecyclerViewAdapterBase;
import cz.uhk.hausy.ui.base.holder.BaseHolder;
import cz.uhk.hausy.ui.base.holder.BaseHolder_;
import cz.uhk.hausy.ui.views.ViewWrapper;

/**
 * Created by kristyna on 07/07/2017.
 */

@EBean
public class BaseRecyclerAdapter<Item extends ISelectable> extends RecyclerViewAdapterBase<Item, BaseHolder> {

    @RootContext
    Context context;

    private boolean selectionEnabled;
    private BaseRecyclerListener baseRecyclerListener;

    @Override
    protected BaseHolder onCreateItemView(ViewGroup parent, int viewType) {
        return BaseHolder_.build(context);
    }

    @Override
    public void onBindViewHolder(final ViewWrapper<BaseHolder> holder, int position) {
        final BaseHolder view = holder.getView();
        final Item selectable = getItems().get(position);

        view.bind(selectable.getSelectable(), selectionEnabled);

        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (selectionEnabled)
                    return false;

                selectionEnabled = true;
                view.toggleChecked();
                notifyDataSetChanged();
                baseRecyclerListener.onStartEdit();
                return true;
            }
        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectionEnabled)
                    view.toggleChecked();
                else
                    baseRecyclerListener.onClick(selectable);
            }
        });
    }

    public void setBaseRecyclerListener(BaseRecyclerListener baseRecyclerListener) {
        this.baseRecyclerListener = baseRecyclerListener;
    }

    public void endEdit() {
        selectionEnabled = false;
        notifyDataSetChanged();
    }

}
