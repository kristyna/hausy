package cz.uhk.hausy.ui.views;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.widget.Toast;

import cz.uhk.hausy.R;
import cz.uhk.hausy.ui.settings.adapters.ThemeAdapter;

/**
 * Created by kristyna on 08/08/2017.
 */

public class ThemePicker extends ListPreference {

    private final SharedPreferences prefs;

    public ThemePicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Override
    protected void onPrepareDialogBuilder(AlertDialog.Builder builder) {
        ThemeAdapter adapter = new ThemeAdapter(getContext());

        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                prefs.edit().putString(getContext().getString(R.string.key_theme), String.valueOf(getEntries()[i])).apply();
            }
        });
    }

}
