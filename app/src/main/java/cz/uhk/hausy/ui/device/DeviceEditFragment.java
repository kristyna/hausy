package cz.uhk.hausy.ui.device;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Select;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ItemSelect;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.ColorRes;
import org.androidannotations.annotations.res.StringArrayRes;
import org.androidannotations.annotations.res.StringRes;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.hausy.R;
import cz.uhk.hausy.common.DeviceType;
import cz.uhk.hausy.interfaces.DialogButtonListener;
import cz.uhk.hausy.interfaces.SpinnerComparator;
import cz.uhk.hausy.io.model.Device;
import cz.uhk.hausy.io.net.requests.CUDeviceRequest;
import cz.uhk.hausy.io.net.requests.DeleteRequest;
import cz.uhk.hausy.ui.base.BaseFragment;
import cz.uhk.hausy.ui.base.MainActivity;
import cz.uhk.hausy.ui.device.picker.IconDialog;
import cz.uhk.hausy.ui.device.picker.IconDialog_;
import cz.uhk.hausy.ui.device.picker.IconPicker;
import cz.uhk.hausy.utils.DialogUtils;
import cz.uhk.hausy.utils.GeneralUtils;
import cz.uhk.hausy.utils.RealmHelper;

/**
 * Created by kristyna on 04/08/2017.
 */

@EFragment(R.layout.fragment_device_new)
@OptionsMenu(R.menu.menu_options)
public class DeviceEditFragment extends BaseFragment implements IconPicker.IconPickerListener {

    private cz.uhk.hausy.io.model.DeviceType selectedType;

    @FragmentArg
    Device device;

    @ViewById
    @NotEmpty
    EditText name;

    @ViewById
    @NotEmpty
    EditText description;

    @ViewById
    EditText icon;

    @ViewById
    @Select
    Spinner type;

    @ViewById
    AutoCompleteTextView unit;

    @ViewById
    EditText min;

    @ViewById
    EditText max;

    @ViewById
    EditText step;

    @StringRes(R.string.choose_device) String devicePrompt;
    @StringArrayRes(R.array.iconPicker) String[] icons;
    @StringArrayRes(R.array.units) String[] units;
    @ColorRes(R.color.textHintLight) int hintColor;
    @ColorRes(R.color.textDark) int textColor;

    @ItemSelect(R.id.type)
    void typeItemSelect(boolean selected, cz.uhk.hausy.io.model.DeviceType type) {
        if (selected)
            selectedType = type;

        switch (type.getId()) {
            case SENSOR:
                setViewVisibility(true, true, true, true);
                break;

            case NONE:
            case SWITCH:
            case BUTTON:
            case MOTION:
                setViewVisibility(false, false, false, false);
                break;
        }
    }

    @OptionsItem
    void delete() {
        if (device == null)
            return;

        DialogUtils.showDeleteDialog(
                getActivity(),
                device.getName(),
                getString(R.string.device),
                new DialogButtonListener() {
                    @Override
                    public void positiveButtonClick() {
                        DeleteRequest request = new DeleteRequest(device.getId(), DeleteRequest.DeleteType.DEVICE);

                        getManager().execute(request, new RequestListener<Void>() {
                            @Override
                            public void onRequestFailure(SpiceException spiceException) {
                                // TODO
                            }

                            @Override
                            public void onRequestSuccess(Void aVoid) {
                                RealmHelper.delete(getRealm(), device);
                                getBaseActivity().onBackPressed();
                            }
                        });
                    }
                }
        );
    }

    @Click
    void icon() {
        IconDialog dialog = IconDialog_.builder().build();
        dialog.setListener(this);
        dialog.show(getBaseActivity().getSupportFragmentManager(), "dialog");
    }

    @Click
    void unit() {
        unit.showDropDown();
    }

    @Click
    void save() {
        getValidator().validate();
    }

    @Override
    public void onValidationSucceeded() {
        if (selectedType.getId() != DeviceType.SENSOR) {
            unit.setText("");
            min.setText("");
            max.setText("");
            step.setText("");
        }

        CUDeviceRequest request = new CUDeviceRequest(
                name.getText().toString(),
                description.getText().toString(),
                selectedType.getId().getValue(),
                icon.getText().toString(),
                unit.getText().toString(),
                GeneralUtils.getDoubleFromString(min),
                GeneralUtils.getDoubleFromString(max),
                GeneralUtils.getDoubleFromString(step)
        );

        if (device != null)
            request.setId(device.getId());


        getManager().execute(request, new RequestListener<Device>() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {
                // TODO
            }

            @Override
            public void onRequestSuccess(Device device) {
                RealmHelper.insertOrUpdate(getRealm(), device);
                getBaseActivity().onBackPressed();
            }
        });
    }

    @AfterViews
    void init() {
        setHasOptionsMenu(true);
        ((MainActivity)getBaseActivity()).changeActionBarIcon(R.drawable.ic_chevron_left);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.item_spinner, units);
        unit.setAdapter(adapter);

        setAdapter();

        if (device != null)
            initDevice();
    }

    private void initDevice() {
        name.setText(device.getName());
        description.setText(device.getDescription());
        icon.setText(device.getIcon());
        unit.setText(device.getUnit());
        min.setText(GeneralUtils.getStringFromDouble(device.getMin()));
        max.setText(GeneralUtils.getStringFromDouble(device.getMax()));
        step.setText(GeneralUtils.getStringFromDouble(device.getStep()));

        GeneralUtils.spinnerSetSelection(type, new SpinnerComparator() {
            @Override
            public boolean equals(int position) {
                return ((cz.uhk.hausy.io.model.DeviceType)type.getAdapter().getItem(position)).getId().getValue() == device.getType();
            }
        });
    }

    private void setAdapter() {
        List<cz.uhk.hausy.io.model.DeviceType> types = new ArrayList<>();
        types.add(new cz.uhk.hausy.io.model.DeviceType(DeviceType.NONE, devicePrompt));
        types.add(new cz.uhk.hausy.io.model.DeviceType(DeviceType.SENSOR, "Sensor"));
        types.add(new cz.uhk.hausy.io.model.DeviceType(DeviceType.SWITCH, "Switch"));
        types.add(new cz.uhk.hausy.io.model.DeviceType(DeviceType.BUTTON, "Button"));
        types.add(new cz.uhk.hausy.io.model.DeviceType(DeviceType.MOTION, "Motion sensor"));

        type.setAdapter(new ArrayAdapter<cz.uhk.hausy.io.model.DeviceType>(getContext(), R.layout.item_spinner, types) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);

                ((TextView)view).setTextColor( position == 0 ? hintColor : textColor );

                return view;
            }
        });
    }

    private void setViewVisibility(boolean vUnit, boolean vMin, boolean vMax, boolean vStep) {
        unit.setVisibility( vUnit ? View.VISIBLE : View.GONE );
        min.setVisibility( vMin ? View.VISIBLE : View.GONE );
        max.setVisibility( vMax ? View.VISIBLE : View.GONE );
        step.setVisibility( vStep ? View.VISIBLE : View.GONE );
    }

    @Override
    public void onIconChange(String icon) {
        this.icon.setText(icon);
    }

    @Override
    public String getSelectedIcon() {
        return icon.getText().toString();
    }

}
