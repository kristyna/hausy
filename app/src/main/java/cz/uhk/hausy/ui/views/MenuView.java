package cz.uhk.hausy.ui.views;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.ColorRes;
import org.androidannotations.annotations.res.DrawableRes;
import org.androidannotations.annotations.res.StringArrayRes;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.util.ArrayList;
import java.util.List;

import cz.uhk.hausy.R;
import cz.uhk.hausy.common.Action;
import cz.uhk.hausy.interfaces.MenuListener;
import cz.uhk.hausy.interfaces.preferences.UserPrefs_;
import cz.uhk.hausy.ui.alexa.AlexaActivity;
import cz.uhk.hausy.ui.alexa.AlexaActivity_;
import cz.uhk.hausy.ui.base.BaseFragment;
import cz.uhk.hausy.ui.base.menu.DrawerAdapter;
import cz.uhk.hausy.ui.base.menu.DrawerItem;
import cz.uhk.hausy.ui.base.menu.SimpleItem;
import cz.uhk.hausy.ui.dashboard.DashboardFragment_;
import cz.uhk.hausy.ui.device.DeviceFragment_;
import cz.uhk.hausy.ui.settings.SettingsActivity_;
import cz.uhk.hausy.ui.sign.LogInActivity_;
import cz.uhk.hausy.ui.user.UserFragment_;
import cz.uhk.hausy.utils.DesignUtils;
import cz.uhk.hausy.utils.design.LinearDecoration;

/**
 * Created by kristyna on 26/06/2017.
 */

@EViewGroup(R.layout.view_menu)
public class MenuView extends RelativeLayout implements DrawerAdapter.OnItemSelectedListener {

    private DrawerAdapter adapter;
    private static final List<DrawerItem> menuItems = new ArrayList<>();
    private MenuListener menuListener;
    private int background;

    @ViewById
    TextView userName;

    @ViewById
    TextView userRole;

    @ViewById(R.id.background)
    ImageView bgImage;

    @ViewById
    RecyclerView menuRecycler;

    @Pref
    UserPrefs_ userPrefs;


    @ColorRes int textLight;
    @ColorRes int textMenu;
    @StringArrayRes String[] menuTitles;
    @StringArrayRes String[] menuIcons;

    @Click
    void logoutClicked() {
        userPrefs.clear();
        LogInActivity_.intent(getContext()).flags(Intent.FLAG_ACTIVITY_CLEAR_TOP).action(Action.LOG_IN).start();
    }

    @AfterViews
    void init() {
        if (menuItems.size() == 0)
            initMenuItems();

        background = DesignUtils.fetchResourceFromTheme(getContext(), R.attr.backgroundTL_BR);
        userName.setText(userPrefs.username().get());
        userRole.setText(userPrefs.role().get());

        Picasso.with(getContext())
                .load(R.drawable.background_small_min)
                .fit().centerCrop()
                .into(bgImage);

        adapter = new DrawerAdapter(menuItems);
        adapter.setListener(this);

        menuRecycler.setNestedScrollingEnabled(false);
        menuRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        menuRecycler.addItemDecoration(new LinearDecoration(getContext(), 10));
        menuRecycler.setAdapter(adapter);

        setBackgroundResource(background);
    }

    @Override
    public void onItemSelected(int position) {
        BaseFragment fragment = null;
        switch (position) {
            case 0: fragment = DashboardFragment_.builder().build(); break;
            case 1: fragment = UserFragment_.builder().build(); break;
            case 2: break;
            case 3: fragment = DeviceFragment_.builder().build(); break;
            case 4: AlexaActivity_.intent(getContext()).start(); return;
            case 5: SettingsActivity_.intent(getContext()).start(); return;

            default:
                Log.d("hausy", "Some case is probably missing | position: " + position);
        }

        if (fragment != null)
            menuListener.changeFragment(fragment);
    }

    private void initMenuItems() {
        if (menuIcons.length != menuTitles.length)
            Log.d("hausy", "Menu icons and titles has different size");

        for (int i = 0; i < menuTitles.length; i++)
            menuItems.add(new SimpleItem(menuIcons[i], menuTitles[i]));
    }

    public MenuView(Context context) {
        super(context);

        setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

    public void setMenuListener(MenuListener menuListener) {
        this.menuListener = menuListener;
    }

}