package cz.uhk.hausy.ui.sign;

import android.widget.EditText;

import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import cz.uhk.hausy.R;
import cz.uhk.hausy.ui.base.BaseFragment;

@EFragment(R.layout.fragment_sign_up)
public class SignUpFragment extends BaseFragment {

    @NotEmpty
    @ViewById
    EditText username;

    @Password
    @ViewById
    EditText password;

    @ConfirmPassword
    @ViewById
    EditText passwordConfirm;

    @Click
    void signIn() {
        getValidator().validate();
    }

    @AfterViews
    void init() {

    }

    @Override
    public void onValidationSucceeded() {
        // TODO api request
        // TODO go to login
        makeToast("success");
    }

}
