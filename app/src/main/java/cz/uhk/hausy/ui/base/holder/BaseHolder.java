package cz.uhk.hausy.ui.base.holder;

import android.content.Context;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import cz.uhk.hausy.R;
import cz.uhk.hausy.io.model.Selectable;

/**
 * Created by kristyna on 16/07/2017.
 */

@EViewGroup(R.layout.item_recycler)
public class BaseHolder extends RelativeLayout {

    private Selectable selectable;

    @ViewById
    CheckBox checkbox;

    @ViewById
    TextView name;

    @ViewById
    TextView description;

    public BaseHolder(Context context) {
        super(context);
    }

    public void bind(Selectable selectable, boolean selectionEnabled) {
        this.selectable = selectable;

        name.setText(selectable.getName());
        description.setText(selectable.getDescription());
        if (selectionEnabled)
            checkbox.setVisibility(VISIBLE);
        else {
            checkbox.setVisibility(GONE);
            checkbox.setChecked(false);
        }
    }

    public void toggleChecked() {
        boolean checked = !checkbox.isChecked();
        selectable.setSelected(checked);
        checkbox.setChecked(checked);
    }

}
