package cz.uhk.hausy.ui.views;

import android.content.Context;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.util.AttributeSet;

import cz.uhk.hausy.R;
import cz.uhk.hausy.utils.DesignUtils;
import cz.uhk.hausy.utils.FontCache;

/**
 * Created by kristyna on 15/09/2017.<br /><br />
 *
 * BaseAutoCompleteTextView handles different typefaces specified in {@link FontCache}.
 * For using specific typeface use custom XML attribute <code>int actvFontType</code>. Possible values and theirs definition can be found in {@link FontCache}.
 */
public class BaseAutoCompleteTextView extends AppCompatAutoCompleteTextView {

    public BaseAutoCompleteTextView(Context context) {
        super(context);
    }

    public BaseAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        setTypeface(DesignUtils.applyCustomFont(context, attrs, R.styleable.BaseAutoCompleteTextView, R.styleable.BaseAutoCompleteTextView_actvFontType));
    }

    public BaseAutoCompleteTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        setTypeface(DesignUtils.applyCustomFont(context, attrs, R.styleable.BaseAutoCompleteTextView, R.styleable.BaseAutoCompleteTextView_actvFontType));
    }

}
