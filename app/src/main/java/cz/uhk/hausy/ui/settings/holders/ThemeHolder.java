package cz.uhk.hausy.ui.settings.holders;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.DimensionPixelSizeRes;

import cz.uhk.hausy.R;
import cz.uhk.hausy.io.model.Selectable;
import cz.uhk.hausy.utils.DesignUtils;

/**
 * Created by kristyna on 08/08/2017.
 */

@EViewGroup(R.layout.item_theme)
public class ThemeHolder extends LinearLayout {

    @ViewById
    View themePreview;

    @ViewById
    TextView themeName;

    @DimensionPixelSizeRes(R.dimen.settings_padding)
    int padding;

    public ThemeHolder(Context context) {
        super(context);
    }

    public ThemeHolder(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ThemeHolder(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @AfterViews
    void init() {
        setPadding(padding, padding/2, 0, padding/2);
    }

    public void bind(String name, int start, int end) {
        GradientDrawable gd = new GradientDrawable(
                GradientDrawable.Orientation.TL_BR,
                new int[] {start,end});
        gd.setCornerRadius(0f);

        themePreview.setBackgroundDrawable(gd);
        themeName.setText(name);
    }

}
