package cz.uhk.hausy.ui.device;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;

import java.util.List;

import cz.uhk.hausy.R;
import cz.uhk.hausy.interfaces.RecyclerFragmentListener;
import cz.uhk.hausy.io.model.Device;
import cz.uhk.hausy.io.model.DeviceType;
import cz.uhk.hausy.io.model.Selectable;
import cz.uhk.hausy.io.net.requests.DevicesRequest;
import cz.uhk.hausy.ui.base.BaseRecyclerFragment;
import cz.uhk.hausy.utils.RealmHelper;
import io.realm.Realm;

/**
 * Created by kristyna on 16/07/2017.
 */

@EFragment(R.layout.fragment_recycler)
public class DeviceFragment extends BaseRecyclerFragment<Device> implements RecyclerFragmentListener<Device> {

    @Click
    void add() {
        getBaseActivity().showFragment(DeviceEditFragment_.builder().build(), true);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRecyclerlistener(this);
    }

    @Override
    public List<Device> getSelectables() {
        return RealmHelper.get(getRealm(), Device.class);
    }

    @Override
    public void onDelete(Selectable selectable) {
        // TODO api
    }

    @Override
    public void onRecyclerItemClick(Device device) {
        getBaseActivity().showFragment(DeviceEditFragment_.builder().device(device).build(), true);
    }

}
