package cz.uhk.hausy.io.net.requests;

import cz.uhk.hausy.io.net.BaseRequest;

/**
 * Created by kristyna on 11/09/2017.
 */

public class CreateGroupRequest extends BaseRequest<String> {

    private final String name, description;

    public CreateGroupRequest(String name, String description) {
        super(String.class);
        this.name = name;
        this.description = description;
    }

    @Override
    protected String loadData() throws Exception {
        return getService().createGroup(name, description).getResponseBody();
    }

}
