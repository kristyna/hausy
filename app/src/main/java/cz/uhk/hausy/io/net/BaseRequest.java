package cz.uhk.hausy.io.net;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by kristyna on 11/09/2017.
 */

public abstract class BaseRequest<T> extends RetrofitSpiceRequest<T, ApiService> {

    public BaseRequest(Class<T> clazz) {
        super(clazz, ApiService.class);
    }

    @Override
    public T loadDataFromNetwork() throws Exception {
        try {
            onPreExecute();
            final T result = loadData();
            onPostExecute(result);
            return result;
        } catch (RetrofitError error) {
            Response response = error.getResponse();
            if (response != null) {
                if (!onError(error)) {
                    int status = response.getStatus();
                    if (status >= 400 && status <= 501) {
                        // Disable retry
                        setRetryPolicy(null);
                    }
                }
            }
            throw error;
        }
    }

    /**
     * Allows performing operation before original request is made.
     */
    protected void onPreExecute() throws Exception {
    }

    /**
     * Allows performing operation after original request is made.
     *
     * @param result
     */
    protected void onPostExecute(T result) throws Exception {
    }

    /**
     * Allows custom handling of error
     *
     * @return false if error is consumed, when true is returned default error policy is applied
     */
    protected boolean onError(RetrofitError error) throws Exception {
        return false;
    }

    protected abstract T loadData() throws Exception;
}
