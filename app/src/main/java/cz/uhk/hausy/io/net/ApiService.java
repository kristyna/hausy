package cz.uhk.hausy.io.net;

import java.util.List;

import cz.uhk.hausy.io.model.Device;
import cz.uhk.hausy.io.model.DeviceType;
import cz.uhk.hausy.io.model.Group;
import cz.uhk.hausy.io.model.User;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;

public interface ApiService {

    String BASE_URL_DEBUG = "http://aa4b74ec.ngrok.io";
    //String BASE_URL_DEBUG = "http://192.168.1.87:8080/";
    String BASE_URL = "https://";
    String BASE_PATH = "/hausyAPI-0.1.0/";
    //String BASE_PATH = "/";

    /**
     * User's requests
     */

    @FormUrlEncoded
    @POST(BASE_PATH+"login/")
    Response<String> login (
            @Field("email") String email,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST(BASE_PATH+"signup/")
    Response<Void> signup (
            @Field("email") String email,
            @Field("password") String password,
            @Field("firstname") String firstname,
            @Field("lastname") String lastname
    );

    @GET(BASE_PATH+"users/")
    Response<List<User>> getUsers();

    @FormUrlEncoded
    @POST(BASE_PATH+"users/")
    Response<Void> createUser (
            @Field("email") String email,
            @Field("password") String password,
            @Field("firstname") String firstname,
            @Field("lastname") String lastname,
            @Field("group") int group
    );

    @GET(BASE_PATH+"users/{id}/")
    Response<User> getUser (
            @Path("id") int id
    );

    @FormUrlEncoded
    @PUT(BASE_PATH+"users/{id}/")
    Response<Void> updateUser (
            @Path("id") int id,
            @Field("email") String email,
            @Field("password") String password,
            @Field("firstname") String firstname,
            @Field("lastname") String lastname,
            @Field("group") int group
    );

    @DELETE(BASE_PATH+"users/{id}/")
    Response<Void> deleteUser (
            @Path("id") int id
    );


    /**
     * Device's requests
     */

    @GET(BASE_PATH+"devices/")
    Response<List<Device>> getDevices();

    @FormUrlEncoded
    @POST(BASE_PATH+"devices/")
    Response<Device> createDevice (
            @Field("name") String name,
            @Field("description") String description,
            @Field("type") int type,
            @Field("icon") String icon,
            @Field("unit") String unit,
            @Field("min") Double min,
            @Field("max") Double max,
            @Field("step") Double step
    );

    @GET(BASE_PATH+"devices/{id}/")
    Response<Device> getDevice (
            @Path("id") int id
    );

    @FormUrlEncoded
    @PUT(BASE_PATH+"devices/{id}")
    Response<Device> updateDevice (
            @Path("id") int id,
            @Field("name") String name,
            @Field("description") String description,
            @Field("type") int type,
            @Field("icon") String icon,
            @Field("unit") String unit,
            @Field("min") Double min,
            @Field("max") Double max,
            @Field("step") Double step
    );

    @DELETE(BASE_PATH+"devices/{id}/")
    Response<Void> deleteDevice (
            @Path("id") int id
    );

    @GET(BASE_PATH+"devices/{id}/value")
    Response<String> getDeviceValue (
            @Path("id") int id
    );

    @FormUrlEncoded
    @PUT(BASE_PATH+"devices/{id}/value")
    Response<String> updateDeviceValue (
            @Path("id") int id,
            @Field("requiredValue") String requiredValue
    );


    /**
     * Group's requests
     */

    @GET(BASE_PATH+"groups/")
    Response<List<Group>> getGroups();

    @FormUrlEncoded
    @POST(BASE_PATH+"groups/")
    Response<String> createGroup (
            @Field("name") String name,
            @Field("description") String description
    );

    @GET(BASE_PATH+"groups/{id}/")
    Response<Group> getGroup (
            @Path("id") int id
    );

    @FormUrlEncoded
    @PUT(BASE_PATH+"groups/{id}/")
    Response<Void> updateGroup (
            @Path("id") int id,
            @Field("name") String name,
            @Field("description") String description
    );

    @DELETE(BASE_PATH+"groups/{id}/")
    Response<Void> deleteGroup (
            @Path("id") int id
    );

}