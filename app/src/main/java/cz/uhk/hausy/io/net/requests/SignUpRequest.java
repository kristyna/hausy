package cz.uhk.hausy.io.net.requests;

import cz.uhk.hausy.io.net.BaseRequest;

/**
 * Created by kristyna on 11/09/2017.
 */

public class SignUpRequest extends BaseRequest<Void> {

    private final String email, password, firstname, lastname;

    public SignUpRequest(final String email, final String password, final String firstname, final String lastname) {
        super(Void.class);
        this.email = email;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    @Override
    protected Void loadData() throws Exception {
        return getService().signup(email, password, firstname, lastname).getResponseBody();
    }

}
