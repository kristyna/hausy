package cz.uhk.hausy.io.net.requests;

import cz.uhk.hausy.io.net.BaseRequest;

/**
 * Created by kristyna on 11/09/2017.
 */

public class CreateUserRequest extends BaseRequest<Void> {

    private final String email, password, firstname, lastname;
    private final int group;

    public CreateUserRequest(String email, String password, String firstname, String lastname, int group) {
        super(Void.class);
        this.email = email;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.group = group;
    }

    @Override
    protected Void loadData() throws Exception {
        return getService().createUser(email, password, firstname, lastname, group).getResponseBody();
    }

}
