package cz.uhk.hausy.io.net.requests;

import java.util.List;

import cz.uhk.hausy.io.model.Device;
import cz.uhk.hausy.io.model.User;
import cz.uhk.hausy.io.net.BaseRequest;

/**
 * Created by kristyna on 12/09/2017.
 */

public class DevicesRequest extends BaseRequest<List> {

    public DevicesRequest() {
        super(List.class);
    }

    @Override
    protected List<Device> loadData() throws Exception {
        return getService().getDevices().getResponseBody();
    }

}
