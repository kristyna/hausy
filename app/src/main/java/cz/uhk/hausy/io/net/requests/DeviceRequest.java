package cz.uhk.hausy.io.net.requests;

import cz.uhk.hausy.io.model.Device;
import cz.uhk.hausy.io.model.User;
import cz.uhk.hausy.io.net.BaseRequest;

/**
 * Created by kristyna on 12/09/2017.
 */

public class DeviceRequest extends BaseRequest<Device> {

    private final int id;

    public DeviceRequest(final int id) {
        super(Device.class);
        this.id = id;
    }

    @Override
    protected Device loadData() throws Exception {
        return getService().getDevice(id).getResponseBody();
    }

}
