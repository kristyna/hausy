package cz.uhk.hausy.io.model;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;
import org.parceler.Transient;

import cz.uhk.hausy.interfaces.ISelectable;
import cz.uhk.hausy.interfaces.TestId;
import cz.uhk.hausy.utils.GenericRealmListParcelConverter;
import io.realm.DeviceRealmProxy;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by kristyna on 05/07/2017.
 */

@Parcel(implementations = { DeviceRealmProxy.class },
        value = Parcel.Serialization.BEAN,
        analyze = { Device.class })
public class Device extends RealmObject implements ISelectable, TestId {

    @PrimaryKey
    private int id;

    private int type, timer;
    private String name, description, requiredValue, unit, icon;
    private Double min, max, step;
    private RealmList<History> history;

    @Transient
    @Ignore
    private transient Selectable selectable = new Selectable(name, description);

    public Device() {}

    public Device(int id, int type, String name, String description) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getType() {
        return type;
    }

    public String getRequiredValue() {
        return requiredValue;
    }

    public String getUnit() {
        return unit;
    }

    public String getIcon() {
        return icon;
    }

    public Double getMin() {
        return min;
    }

    public Double getMax() {
        return max;
    }

    public Double getStep() {
        return step;
    }

    public int getTimer() {
        return timer;
    }

    public RealmList<History> getHistory() {
        return history;
    }

    @ParcelPropertyConverter(GenericRealmListParcelConverter.class)
    public void setHistory(RealmList<History> history) {
        this.history = history;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public Selectable getSelectable() {
        return selectable;
    }

    public void prepare() {
        selectable = new Selectable(name, description);
    }

}
