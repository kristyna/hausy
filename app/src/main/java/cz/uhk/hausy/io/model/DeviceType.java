package cz.uhk.hausy.io.model;

/**
 * Created by kristyna on 13/09/2017.
 */

public class DeviceType {

    private cz.uhk.hausy.common.DeviceType id;
    private String name;

    public DeviceType() {}

    public DeviceType(cz.uhk.hausy.common.DeviceType id, String name) {
        this.id = id;
        this.name = name;
    }

    public cz.uhk.hausy.common.DeviceType getId() {
        return id;
    }

    @Override
    public String toString() {
       return name;
    }

}
