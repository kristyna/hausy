package cz.uhk.hausy.io.net.requests;

import cz.uhk.hausy.io.net.BaseRequest;

/**
 * Created by kristyna on 11/09/2017.
 */

public class LoginRequest extends BaseRequest<String> {

    private String email;
    private String password;

    public LoginRequest(String email, String password) {
        super(String.class);
        this.email = email;
        this.password = password;
    }

    @Override
    protected String loadData() throws Exception {
        return getService().login(email, password).getResponseBody();
    }

}
