package cz.uhk.hausy.io.model;

import org.parceler.Parcel;
import org.parceler.Transient;

import cz.uhk.hausy.interfaces.ISelectable;
import cz.uhk.hausy.interfaces.TestId;
import io.realm.RealmObject;
import io.realm.UserRealmProxy;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by kristyna on 07/07/2017.
 */

@Parcel(implementations = { UserRealmProxy.class },
        value = Parcel.Serialization.BEAN,
        analyze = { User.class })
public class User extends RealmObject implements ISelectable, TestId {

    @PrimaryKey
    private int id;

    private String email;
    private String firstname;
    private String lastname;
    private Group group;

    @Transient
    @Ignore
    private transient Selectable selectable = new Selectable(firstname + " " + lastname, email);

    public User() {}

    public User(String email, String firstname, String lastname, Group group) {
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
        this.group = group;
    }


    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public Group getGroup() {
        return group;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public void update(String email, String firstname, String lastname, Group group) {
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
        this.group = group;
    }


    @Override
    public Selectable getSelectable() {
        return selectable;
    }

}
