package cz.uhk.hausy.io.net.requests;

import android.util.Log;

import cz.uhk.hausy.io.model.Device;
import cz.uhk.hausy.io.model.DeviceType;
import cz.uhk.hausy.io.net.BaseRequest;

/**
 * Created by kristyna on 11/09/2017.
 */

public class CUDeviceRequest extends BaseRequest<Device> {

    private int id, type;
    private final String name, description, icon, unit;
    private final Double min, max, step;

    public CUDeviceRequest(String name, String description, int type, String icon, String unit, Double min, Double max, Double step) {
        super(Device.class);
        this.name = name;
        this.description = description;
        this.type = type;
        this.icon = icon;
        this.unit = unit;
        this.min = min;
        this.max = max;
        this.step = step;
    }

    @Override
    protected Device loadData() throws Exception {
        if (id == 0)
            return getService().createDevice(name, description, type, icon, unit, min, max, step).getResponseBody();
        else
            return getService().updateDevice(id, name, description, type, icon, unit, min, max, step).getResponseBody();
    }

    public void setId(int id) {
        this.id = id;
    }

}
