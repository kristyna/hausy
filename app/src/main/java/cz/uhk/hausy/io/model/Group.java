package cz.uhk.hausy.io.model;

import org.parceler.Parcel;
import org.parceler.Transient;

import cz.uhk.hausy.interfaces.ISelectable;
import io.realm.GroupRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by kristyna on 05/07/2017.
 */

@Parcel(implementations = { GroupRealmProxy.class },
        value = Parcel.Serialization.BEAN,
        analyze = { Group.class })
public class Group extends RealmObject implements ISelectable {

    @PrimaryKey
    private int id;

    private String name, description;

    @Transient
    @Ignore
    private transient Selectable selectable = new Selectable(name, description);

    public Group() {}

    public Group(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public Selectable getSelectable() {
        return selectable;
    }

}
