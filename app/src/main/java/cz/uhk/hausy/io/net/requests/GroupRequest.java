package cz.uhk.hausy.io.net.requests;

import cz.uhk.hausy.io.model.Group;
import cz.uhk.hausy.io.model.User;
import cz.uhk.hausy.io.net.BaseRequest;

/**
 * Created by kristyna on 12/09/2017.
 */

public class GroupRequest extends BaseRequest<Group> {

    private final int id;

    public GroupRequest(final int id) {
        super(Group.class);
        this.id = id;
    }

    @Override
    protected Group loadData() throws Exception {
        return getService().getGroup(id).getResponseBody();
    }

}
