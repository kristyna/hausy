package cz.uhk.hausy.io.net.requests;

import cz.uhk.hausy.io.net.BaseRequest;

/**
 * Created by kristyna on 12/09/2017.
 */

public class DeviceValueRequest extends BaseRequest<String> {

    private final int id;

    public DeviceValueRequest(final int id) {
        super(String.class);
        this.id = id;
    }

    @Override
    protected String loadData() throws Exception {
        return getService().getDeviceValue(id).getResponseBody();
    }

}
