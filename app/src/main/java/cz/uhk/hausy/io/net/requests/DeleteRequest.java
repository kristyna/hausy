package cz.uhk.hausy.io.net.requests;

import cz.uhk.hausy.io.net.BaseRequest;

/**
 * Created by kristyna on 12/09/2017.
 */

public class DeleteRequest extends BaseRequest<Void> {

    public enum DeleteType {
        USER, DEVICE, GROUP
    }

    private final int id;
    private final DeleteType type;

    public DeleteRequest(final int id, final DeleteType type) {
        super(Void.class);
        this.id = id;
        this.type = type;
    }

    @Override
    protected Void loadData() throws Exception {
        switch (type) {
            case USER:
                return getService().deleteUser(id).getResponseBody();

            case DEVICE:
                return getService().deleteDevice(id).getResponseBody();

            case GROUP:
                return getService().deleteGroup(id).getResponseBody();
        }

        return null;
    }

}
