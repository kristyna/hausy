package cz.uhk.hausy.io.model;

import com.jjoe64.graphview.series.DataPointInterface;

import org.parceler.Parcel;

import java.util.Date;

import cz.uhk.hausy.utils.GeneralUtils;
import io.realm.HistoryRealmProxy;
import io.realm.RealmObject;
import io.realm.UserRealmProxy;

/**
 * Created by kristyna on 20/09/2017.
 *
 * TODO
 * Model holds every change what happend to an device.
 * Know who changed it, when and which value user set up.
 */

@Parcel(implementations = { HistoryRealmProxy.class },
        value = Parcel.Serialization.BEAN,
        analyze = { History.class })
public class History extends RealmObject implements DataPointInterface {

    private int userId;
    private Date date;
    private String value;

    public History() {}

    public History(int userId, Date date, String value) {
        this.userId = userId;
        this.date = date;
        this.value = value;
    }

    @Override
    public double getX() {
        return date.getTime();
    }

    @Override
    public double getY() {
        return Double.valueOf(value);
    }

}
