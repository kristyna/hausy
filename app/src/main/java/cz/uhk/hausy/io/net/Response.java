package cz.uhk.hausy.io.net;

/**
 * Created by kristyna on 11/09/2017.
 */

public class Response<T> {

    private int code;
    private T response;
    private String message;

    public int getCode() {
        return code;
    }

    public T getResponseBody() {
        return response;
    }

    public String getMessage() {
        return message;
    }

}
