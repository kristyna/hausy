package cz.uhk.hausy.io.net.requests;

import java.util.List;

import cz.uhk.hausy.io.model.Group;
import cz.uhk.hausy.io.model.User;
import cz.uhk.hausy.io.net.BaseRequest;

/**
 * Created by kristyna on 12/09/2017.
 */

public class GroupsRequest extends BaseRequest<List> {

    public GroupsRequest() {
        super(List.class);
    }

    @Override
    protected List<Group> loadData() throws Exception {
        return getService().getGroups().getResponseBody();
    }

}
