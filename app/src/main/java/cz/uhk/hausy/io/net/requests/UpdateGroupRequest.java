package cz.uhk.hausy.io.net.requests;

import cz.uhk.hausy.io.net.BaseRequest;

/**
 * Created by kristyna on 11/09/2017.
 */

public class UpdateGroupRequest extends BaseRequest<Void> {

    private final int id;
    private final String name, description;

    public UpdateGroupRequest(int id, String name, String description) {
        super(Void.class);
        this.id = id;
        this.name = name;
        this.description = description;
    }

    @Override
    protected Void loadData() throws Exception {
        return getService().updateGroup(id, name, description).getResponseBody();
    }

}
