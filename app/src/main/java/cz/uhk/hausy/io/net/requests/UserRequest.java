package cz.uhk.hausy.io.net.requests;

import java.util.List;

import cz.uhk.hausy.io.model.User;
import cz.uhk.hausy.io.net.BaseRequest;

/**
 * Created by kristyna on 12/09/2017.
 */

public class UserRequest extends BaseRequest<User> {

    private final int id;

    public UserRequest(final int id) {
        super(User.class);
        this.id = id;
    }

    @Override
    protected User loadData() throws Exception {
        return getService().getUser(id).getResponseBody();
    }

}
