package cz.uhk.hausy.io.net.requests;

import java.util.List;

import cz.uhk.hausy.io.model.User;
import cz.uhk.hausy.io.net.BaseRequest;

/**
 * Created by kristyna on 12/09/2017.
 */

public class UsersRequest extends BaseRequest<List> {

    public UsersRequest() {
        super(List.class);
    }

    @Override
    protected List<User> loadData() throws Exception {
        return getService().getUsers().getResponseBody();
    }

}
