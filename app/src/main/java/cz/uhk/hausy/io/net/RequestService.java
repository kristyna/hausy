package cz.uhk.hausy.io.net;

import android.app.Application;
import android.util.Log;

import com.google.gson.GsonBuilder;
import com.octo.android.robospice.SpiceService;
import com.octo.android.robospice.persistence.CacheManager;
import com.octo.android.robospice.persistence.exception.CacheCreationException;
import com.octo.android.robospice.persistence.retrofit.GsonRetrofitObjectPersisterFactory;
import com.octo.android.robospice.request.CachedSpiceRequest;
import com.octo.android.robospice.request.listener.RequestListener;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EService;

import java.util.Set;

import cz.uhk.hausy.BuildConfig;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;
import roboguice.util.temp.Ln;

@EService
public class RequestService extends SpiceService {

    private static GsonConverter GSON_CONVERTER = new GsonConverter(new GsonBuilder()
            .setDateFormat("yyyy-MM-dd HH:mm:ss")
            .create());

    private ApiService mService;

    public ApiService buildService() {
        return new RestAdapter.Builder()
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                .setEndpoint(BuildConfig.DEBUG ? ApiService.BASE_URL_DEBUG : ApiService.BASE_URL)
                .setConverter(GSON_CONVERTER)
                .build()
                .create(ApiService.class);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Ln.getConfig().setLoggingLevel(Log.ERROR);
        mService = buildService();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void addRequest(CachedSpiceRequest<?> request, Set<RequestListener<?>> listRequestListener) {
        if (request.getSpiceRequest() instanceof RetrofitSpiceRequest) {
            RetrofitSpiceRequest retrofitSpiceRequest = (RetrofitSpiceRequest) request.getSpiceRequest();
            retrofitSpiceRequest.setService(mService);
        }

        super.addRequest(request, listRequestListener);
    }

    @Override
    public CacheManager createCacheManager(Application application) throws CacheCreationException {
        CacheManager cacheManager = new CacheManager();
        cacheManager.addPersister(new GsonRetrofitObjectPersisterFactory(application, GSON_CONVERTER, null));
        return cacheManager;
    }

    @Override
    public int getThreadCount() {
        return 5;
    }

}