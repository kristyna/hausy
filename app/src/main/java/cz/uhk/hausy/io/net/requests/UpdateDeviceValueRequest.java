package cz.uhk.hausy.io.net.requests;

import cz.uhk.hausy.io.net.BaseRequest;

/**
 * Created by kristyna on 12/09/2017.
 */

public class UpdateDeviceValueRequest extends BaseRequest<String> {

    private final int id;
    private final String value;

    public UpdateDeviceValueRequest(final int id, final String value) {
        super(String.class);
        this.id = id;
        this.value = value;
    }

    @Override
    protected String loadData() throws Exception {
        return getService().updateDeviceValue(id, value).getResponseBody();
    }

}
