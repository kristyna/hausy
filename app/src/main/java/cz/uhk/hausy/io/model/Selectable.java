package cz.uhk.hausy.io.model;

import io.realm.RealmObject;

/**
 * Created by kristyna on 17/07/2017.
 */

public class Selectable {

    private boolean selected;
    private String name = "";
    private String description = "";

    public Selectable(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

}
