package cz.uhk.hausy.interfaces;

/**
 * Created by kristyna on 13/09/2017.
 */

public interface SpinnerComparator {

    boolean equals(int position);

}
