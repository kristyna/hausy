package cz.uhk.hausy.interfaces;

/**
 * Created by kristyna on 05/07/2017.
 */

public interface ItemTouchListener {

    boolean onItemMove(int from, int to);
    void onItemDismiss(int position);

}
