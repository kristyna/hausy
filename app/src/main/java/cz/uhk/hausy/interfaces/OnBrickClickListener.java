package cz.uhk.hausy.interfaces;

import cz.uhk.hausy.io.model.Device;

/**
 * Created by kristyna on 05/07/2017.
 */

public interface OnBrickClickListener {

    void onValueClick(Device device);
    void onSwitchClick(Device device);
    void onButtonClick(Device device);
    void onLightsClick(Device device);

}
