package cz.uhk.hausy.interfaces;

/**
 * Created by kristyna on 14/09/2017.
 */

public abstract class DialogButtonListener {

    public void positiveButtonClick() {}
    public void negativeButtonClick() {}

}
