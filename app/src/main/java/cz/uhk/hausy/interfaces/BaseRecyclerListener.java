package cz.uhk.hausy.interfaces;

import cz.uhk.hausy.io.model.Selectable;

/**
 * Created by kristyna on 13/07/2017.
 */

public interface BaseRecyclerListener<T> {

    void onClick(T selectable);
    void onStartEdit();

}
