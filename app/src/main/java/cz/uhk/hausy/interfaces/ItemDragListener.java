package cz.uhk.hausy.interfaces;

import cz.uhk.hausy.ui.views.ViewWrapper;

/**
 * Created by kristyna on 05/07/2017.
 */

public interface ItemDragListener {

    void drag(ViewWrapper viewHolder);

}
