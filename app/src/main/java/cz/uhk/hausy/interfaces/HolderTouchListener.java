package cz.uhk.hausy.interfaces;

/**
 * Created by kristyna on 05/07/2017.
 */

public interface HolderTouchListener {

    void onItemSelected();
    void onItemClear();
    void onItemDropped();

}
