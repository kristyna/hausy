package cz.uhk.hausy.interfaces;

import cz.uhk.hausy.io.model.Selectable;

/**
 * Created by kristyna on 13/09/2017.
 */

public interface ISelectable {

    Selectable getSelectable();

}
