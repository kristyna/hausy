package cz.uhk.hausy.interfaces;

/**
 * Created by kristyna on 26/06/17.
 */

public interface OnBackPressedListener {

    void backPressed();

}
