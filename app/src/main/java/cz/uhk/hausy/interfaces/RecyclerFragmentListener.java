package cz.uhk.hausy.interfaces;

import java.util.List;

import cz.uhk.hausy.io.model.Selectable;

/**
 * Created by kristyna on 17/07/2017.
 */

public interface RecyclerFragmentListener<T> {

    List<T> getSelectables();
    void onDelete(Selectable selectable);
    void onRecyclerItemClick(T selectable);

}
