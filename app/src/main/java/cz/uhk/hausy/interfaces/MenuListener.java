package cz.uhk.hausy.interfaces;

import cz.uhk.hausy.ui.base.BaseFragment;

/**
 * Created by kristyna on 07/07/2017.
 */

public interface MenuListener {

    void changeFragment(BaseFragment fragment);

}
