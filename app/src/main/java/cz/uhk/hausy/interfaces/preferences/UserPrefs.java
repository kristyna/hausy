package cz.uhk.hausy.interfaces.preferences;

import org.androidannotations.annotations.sharedpreferences.DefaultString;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Created by kristyna on 30/06/2017.
 */

@SharedPref(value = SharedPref.Scope.APPLICATION_DEFAULT)
public interface UserPrefs {

    @DefaultString("")
    String username();

    @DefaultString("")
    String firstname();

    @DefaultString("")
    String lastname();

    @DefaultString("Unknown")
    String role();

    @DefaultString("")
    String token();

    @DefaultString("[]")
    String dashboard();

}
