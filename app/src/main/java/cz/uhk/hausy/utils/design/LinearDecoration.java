package cz.uhk.hausy.utils.design;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import cz.uhk.hausy.utils.DesignUtils;

/**
 * Created by kristyna on 30/06/2017.
 */

public class LinearDecoration extends RecyclerView.ItemDecoration {

    private final int spacing;
    private final int leftSpacing;

    public LinearDecoration(Context context, int spacing) {
        this.spacing = DesignUtils.convertDpToPixel(context, spacing);
        this.leftSpacing = 0;
    }

    public LinearDecoration(Context context, int spacing, int leftSpacing) {
        this.spacing = DesignUtils.convertDpToPixel(context, spacing);
        this.leftSpacing = DesignUtils.convertDpToPixel(context, leftSpacing);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int orientation = ((LinearLayoutManager)parent.getLayoutManager()).getOrientation();

        if (orientation == LinearLayoutManager.HORIZONTAL) {
            outRect.right = spacing;

            if (parent.getChildAdapterPosition(view) == 0)
                outRect.left = leftSpacing;
        } else
            outRect.bottom = spacing;
    }

}
