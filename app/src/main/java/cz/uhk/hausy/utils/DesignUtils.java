package cz.uhk.hausy.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;

import cz.uhk.hausy.R;

/**
 * Created by kristyna on 26/06/17.
 */

public class DesignUtils {

    private static final String themePref = "theme";
    private static final String defaultTheme = "default";
    private static int[] themes = new int[]{
            R.string.shroomHaze, R.string.moss, R.string.venice_blue,
            R.string.sunrise, R.string.aubergine, R.string.bloody_mary,
            R.string.horizon, R.string.energy
    };
    private static int[] themeStyles = new int[]{
            R.style.Default, R.style.Moss, R.style.Venice,
            R.style.Sunrise, R.style.Aubergine, R.style.Bloody,
            R.style.Horizon, R.style.Energy
    };
    private static int[] themeSplashStyles = new int[]{
            R.style.DefaultSplash, R.style.MossSplash, R.style.VeniceSplash,
            R.style.SunriseSplash, R.style.AubergineSplash, R.style.BloodySplash,
            R.style.HorizonSplash, R.style.EnergySplash
    };

    public static Typeface applyCustomFont(Context context, AttributeSet attrs, int[] styleable, int style) {
        int fonttype;
        if (attrs != null) {
            TypedArray ta = context.obtainStyledAttributes(attrs, styleable, 0, 0);
            try {
                fonttype = ta.getInt(style, 0);
            } finally {
                ta.recycle();
            }
        } else
            fonttype = 0;

        return FontCache.getFont(fonttype);
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param context Context to get resources and device specific display metrics
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static int convertDpToPixel(Context context, float dp){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();

        return (int) (dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static DisplayMetrics getDisplayMetrics(Activity activity) {
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        return metrics;
    }

    public static int fetchColorFromTheme(Context context, int colorId) {
        TypedValue typedValue = new TypedValue();

        TypedArray a = context.obtainStyledAttributes(typedValue.data, new int[] { colorId });
        int color = a.getColor(0, 0);

        a.recycle();

        return color;
    }

    public static int fetchResourceFromTheme(Context context, int resourceId) {
        TypedValue typedValue = new TypedValue();

        TypedArray a = context.obtainStyledAttributes(typedValue.data, new int[] { resourceId });
        int resource = a.getResourceId(0, 0);

        a.recycle();

        return resource;
    }

    public static int getThemeId(Context context) {
        String current = PreferenceManager
                .getDefaultSharedPreferences(context)
                .getString(themePref, defaultTheme);

        for (int i = 0; i < themes.length; i++) {
            if (context.getString(themes[i]).equals(current))
                return themeStyles[i];
        }

        return themeStyles[0];
    }

    public static int getSplashThemeId(Context context) {
        String current = PreferenceManager
                .getDefaultSharedPreferences(context)
                .getString(themePref, defaultTheme);

        for (int i = 0; i < themes.length; i++) {
            if (context.getString(themes[i]).equals(current))
                return themeSplashStyles[i];
        }

        return themeSplashStyles[0];
    }

}
