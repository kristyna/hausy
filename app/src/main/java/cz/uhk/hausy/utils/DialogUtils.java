package cz.uhk.hausy.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import cz.uhk.hausy.R;
import cz.uhk.hausy.interfaces.DialogButtonListener;

/**
 * Created by kristyna on 14/09/2017.
 */

public class DialogUtils {

    private static void showDialog(Activity activity, @StringRes int title, String message, @StringRes int positiveButton, @StringRes int negativeButton, @NonNull final DialogButtonListener listener) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
        dialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton(positiveButton, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listener.positiveButtonClick();
                    }
                })
                .setNegativeButton(negativeButton, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listener.negativeButtonClick();
                    }
                })
                .show();
    }

    public static void showDialog(Activity activity, @StringRes int title, @StringRes int message, @StringRes int positiveButton, @StringRes int negativeButton, @NonNull final DialogButtonListener listener) {
        showDialog(activity, title, activity.getString(message), positiveButton, negativeButton, listener);
    }

    public static void showDeleteDialog(final Activity activity, String deleteParam1, String deleteParam2, @NonNull final DialogButtonListener listener) {
        showDialog(activity, R.string.confirm,
                activity.getString(R.string.dialog_delete_message, deleteParam1, deleteParam2),
                R.string.delete, R.string.cancel, listener);
    }

}
