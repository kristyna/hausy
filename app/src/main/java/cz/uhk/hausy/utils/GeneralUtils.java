package cz.uhk.hausy.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import cz.uhk.hausy.R;
import cz.uhk.hausy.interfaces.DialogButtonListener;
import cz.uhk.hausy.interfaces.SpinnerComparator;

/**
 * Created by kristyna on 17/07/2017.
 */

public class GeneralUtils {

    private static final String EMPTY_STRING = "";
    private static final String NULL_STRING = "null";

    public static boolean contains(String s1, String s2) {
        return s1.toLowerCase().contains(s2.toLowerCase());
    }

    public static void spinnerSetSelection(Spinner spinner, SpinnerComparator comparator) {
        SpinnerAdapter adapter = spinner.getAdapter();
        for (int i = 0; i < adapter.getCount(); i++)
            if (comparator.equals(i))
                spinner.setSelection(i);
    }

    public static String getStringFromDouble(Double number) {
        return (number == null) ? EMPTY_STRING : String.valueOf(number);
    }

    public static Double getDoubleFromString(EditText input) {
        return (input.getText().length() == 0 || input.getText().toString().equals(NULL_STRING)) ?
                null : Double.valueOf(input.getText().toString());
    }

}
