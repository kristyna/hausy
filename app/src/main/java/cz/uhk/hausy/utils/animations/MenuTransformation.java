package cz.uhk.hausy.utils.animations;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.View;

import com.yarolegovich.slidingrootnav.transform.RootTransformation;
import com.yarolegovich.slidingrootnav.util.SideNavUtils;

import cz.uhk.hausy.utils.DesignUtils;

/**
 * Created by kristyna on 26/06/17.
 */

public class MenuTransformation implements RootTransformation {

    private static final float DEFAULT_START = 0f;
    private static final float END_ROTATION = -6.0f;

    private final float translationX;

    public MenuTransformation(Activity activity) {
        DisplayMetrics displayMetrics = DesignUtils.getDisplayMetrics(activity);
        translationX = displayMetrics.widthPixels*0.2f;
    }

    @Override
    public void transform(float dragProgress, View rootView) {
        float rotation = SideNavUtils.evaluate(dragProgress, DEFAULT_START, END_ROTATION);
        float transX = SideNavUtils.evaluate(dragProgress, DEFAULT_START, translationX);
        rootView.setRotationY(rotation);
        rootView.setTranslationX(transX);
    }
}