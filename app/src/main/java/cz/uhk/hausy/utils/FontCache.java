package cz.uhk.hausy.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.LruCache;

import cz.uhk.hausy.common.Constants;

/**
 * Created by kristyna on 26/03/2017.
 */

public class FontCache {

    public static int CODE_LIGHT = 0;
    public static int CODE_BOLD = 1;
    public static int FONT_AWESOME = 2;
    public static int LINTEL_REGULAR = 3;
    public static int HAUSY = 4;

    private static LruCache<Integer, Typeface> typefaceCache;

    public static void initFonts(Context context) {
        typefaceCache = new LruCache<>(5);
        typefaceCache.put(CODE_LIGHT, Typeface.createFromAsset(context.getAssets(), Constants.FONT_LIGHT));
        typefaceCache.put(CODE_BOLD, Typeface.createFromAsset(context.getAssets(), Constants.FONT_BOLD));
        typefaceCache.put(FONT_AWESOME, Typeface.createFromAsset(context.getAssets(), Constants.FONT_AWESOME));
        typefaceCache.put(LINTEL_REGULAR, Typeface.createFromAsset(context.getAssets(), Constants.FONT_REGULAR));
        typefaceCache.put(HAUSY, Typeface.createFromAsset(context.getAssets(), Constants.FONT_HAUSY));
    }

    public static Typeface getFont(int fonttype) {
        return typefaceCache.get(fonttype);
    }

}
