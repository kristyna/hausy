package cz.uhk.hausy.utils;

import android.util.Log;

import java.util.Collection;
import java.util.List;

import cz.uhk.hausy.interfaces.TestId;
import cz.uhk.hausy.io.model.Device;
import io.realm.OrderedRealmCollectionSnapshot;
import io.realm.Realm;
import io.realm.RealmModel;
import io.realm.RealmObject;

/**
 * Created by kristyna on 13/09/2017.
 */

public class RealmHelper {

    public static <T extends RealmObject> List<T> get(Realm realm, Class<T> clazz) {
        return realm.where(clazz).findAll();
    }

    public static void insertOrUpdate(Realm realm, RealmModel model) {
        realm.beginTransaction();
        realm.insertOrUpdate(model);
        realm.commitTransaction();
    }

    public static void insertOrUpdate(Realm realm, Collection<? extends RealmModel> collection) {
        realm.beginTransaction();
        realm.insertOrUpdate(collection);
        realm.commitTransaction();
    }

    public static <T extends RealmObject> int getId(Realm realm, Class<T> clazz) {
        Number id = realm.where(clazz).max("id");
        if (id != null)
            return id.intValue() + 1;
        else
            return 0;
    }

    public static <T extends RealmObject&TestId> T detachFromRealm(Realm realm, Class<T> clazz, T object) {
        if (object == null) {
            try {
                object = clazz.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            object.setId(RealmHelper.getId(realm, Device.class));
        } else
            object = realm.copyFromRealm(object);

        return object;
    }

    public static <T extends RealmObject> void delete(Realm realm, T object) {
        realm.beginTransaction();
        object.deleteFromRealm();
        realm.commitTransaction();
    }

}