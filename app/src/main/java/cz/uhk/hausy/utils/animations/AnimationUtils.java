package cz.uhk.hausy.utils.animations;

import android.content.Context;
import android.support.annotation.AnimRes;
import android.view.View;

import com.daimajia.androidanimations.library.BaseViewAnimator;
import com.daimajia.androidanimations.library.YoYo;

/**
 * Created by kristyna on 06/07/2017.
 */

public class AnimationUtils {

    private static final int BASE_DURATION = 400;

    public static void animate(Context context, View view, @AnimRes int animation) {
        view.startAnimation(
                android.view.animation.AnimationUtils.loadAnimation(context, animation)
        );
    }

    public static void animate(BaseViewAnimator animator, View target) {
        YoYo.with(animator).duration(BASE_DURATION).playOn(target);
    }

}
