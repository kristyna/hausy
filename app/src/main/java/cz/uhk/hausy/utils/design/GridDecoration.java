package cz.uhk.hausy.utils.design;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import cz.uhk.hausy.utils.DesignUtils;

/**
 * Created by kristyna on 30/06/2017.
 */

public class GridDecoration extends RecyclerView.ItemDecoration {

    private final int spacing;
    private final int columns;

    public GridDecoration(Context context, int spacing, int columns) {
        this.spacing = DesignUtils.convertDpToPixel(context, spacing);
        this.columns = columns;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);
        int column = position % columns;

        outRect.left = column * spacing / columns;
        outRect.right = spacing - (column + 1) * spacing / columns;
        outRect.top = spacing;

        if (column == 0)
            outRect.left = spacing;
        else
            outRect.right = spacing;

        int size = parent.getAdapter().getItemCount();
        if (position > size - 1 - columns + size%columns)
            outRect.bottom = spacing;

    }

}
